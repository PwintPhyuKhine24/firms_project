jQuery(function() {
    $('#member_unitprice').change(function() {
      var numAV, numUP, strPji;
      strPji = new String();
      numUP = new Number();
      numAV = new Number();
      numUP = $('#member_unitprice').val();
      numAV = $('#member_availability').val();
      strPji += "<span id='lbl_pji'>※本サイト登録後のあなたの想定年間プロジェクトインカムは、約<span class='text-danger font-weight-bold'>";
      strPji += Math.round(numUP * numAV * 8 * 4 * 12 / 10000);
      strPji += "万円</span>です。<br /></span>";
      $('span#lbl_pji').replaceWith(strPji);
    });
    $('#member_availability').change(function() {
      var numAV, numUP, strPji;
      strPji = new String();
      numUP = new Number();
      numAV = new Number();
      numUP = $('#member_unitprice').val();
      numAV = $('#member_availability').val();
      strPji += "<span id='lbl_pji'>※本サイト登録後のあなたの想定年間プロジェクトインカムは、約<span class='text-danger font-weight-bold'>";
      strPji += Math.round(numUP * numAV * 8 * 4 * 12 / 10000);
      strPji += "万円</span>です。<br /></span>";
      $('span#lbl_pji').replaceWith(strPji);
    });
    $('#firm_add').click(function() {
      if ($('#tr_firm2').css("display") === "none") {
        $('#tr_firm2').toggle("nomal");
      } else if ($('#tr_firm3').css("display") === "none") {
        $('#tr_firm3').toggle("nomal");
      } else if ($('#tr_firm4').css("display") === "none") {
        $('#tr_firm4').toggle("nomal");
      } else if ($('#tr_firm5').css("display") === "none") {
        $('#tr_firm5').toggle("nomal");
        $('#firm_add').css("display", "none");
      }
    });
  
    $('#firm_add_edit').click(function() {
      if ($('#tr_firm2').css("display") === "none") {
          $('#tr_firm2').toggle();
        } else if ($('#tr_firm3').css("display") === "none") {
          $('#tr_firm3').toggle();
        } else if ($('#tr_firm4').css("display") === "none") {
          $('#tr_firm4').toggle();
        } else if ($('#tr_firm5').css("display") === "none") {
          $('#tr_firm5').toggle();
          $('#firm_add_edit').css("display", "none");
        }
    });
    
    $('#base_pji_update').click(function() {
      var i, j, k, numF, numF1, numF2, numF3, numF4, numF5, numFA, numT, numT1, numT2, numT3, numT4, numT5, numTA, strPji;
      strPji = new String();
      numF1 = new Number($('#member_firm1').val());
      numF2 = new Number($('#member_firm2').val());
      numF3 = new Number($('#member_firm3').val());
      numF4 = new Number($('#member_firm4').val());
      numF5 = new Number($('#member_firm5').val());
      numT1 = new Number($('#member_term1').val());
      numT2 = new Number($('#member_term2').val());
      numT3 = new Number($('#member_term3').val());
      numT4 = new Number($('#member_term4').val());
      numT5 = new Number($('#member_term5').val());
      numFA = new Array(numF1, numF2, numF3, numF4, numF5);
      numTA = new Array(numT1, numT2, numT3, numT4, numT5);
      numF = new Number();
      numT = new Number();
      numT = numT1 + numT2 + numT3 + numT4 + numT5;
      for (i = j = 0; j <= 4; i = ++j) {
        if (numFA[i] < 3) {
          numFA[i] = 5 * numTA[i];
        } else if (numFA[i] >= 3 && numFA[i] < 6) {
          numFA[i] = 4.5 * numTA[i];
        } else if (numFA[i] >= 6 && numFA[i] < 17) {
          numFA[i] = 4 * numTA[i];
        } else if (numFA[i] >= 17 && numFA[i] < 26) {
          numFA[i] = 3.5 * numTA[i];
        } else if (numFA[i] >= 26 && numFA[i] < 37) {
          numFA[i] = 3 * numTA[i];
        } else {
          numFA[i] = 1 * numTA[i];
        }
      }
      for (i = k = 0; k <= 4; i = ++k) {
        numF = numF + numFA[i] / numT;
      }
      if (numT < 0.5) {
        numT = 0;
      } else if (numT >= 0.5 && numT < 1) {
        numT = 1;
      } else if (numT >= 1 && numT < 2) {
        numT = 1.5;
      } else if (numT >= 2 && numT < 3) {
        numT = 2;
      } else if (numT >= 3 && numT < 4) {
        numT = 2.5;
      } else if (numT >= 4 && numT < 6) {
        numT = 3;
      } else if (numT >= 6 && numT < 8) {
        numT = 3.5;
      } else if (numT >= 8 && numT < 10) {
        numT = 4;
      } else if (numT >= 10 && numT < 15) {
        numT = 4.5;
      } else {
        numT = 5;
      }
      strPji += "<span id='lbl_base_pji'>あなたの想定年間プロジェクトインカムは、約<span class='text-danger font-weight-bold'>";
      strPji += Math.round((numF + numT) * 1200 * 8 * 3 * 4 * 12 / 10000);
      strPji += "万円</span>です。<br />※週3日稼働を想定。<br /></span>";
      $('span#lbl_base_pji').replaceWith(strPji);
  
      if ($('#reg').css("display") === "none") {
        $('#reg').toggle("nomal");
      }
  
    });
  });
  
  