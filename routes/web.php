<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });
Route::get('/','MainController@index')->name('index');

Route::group(['group' => 'acc'], function(){
    Route::get('/login', 'Auth\LoginController@login')->name('login');
    Route::post('/registration','Auth\RegisterController@createMember')->name('registration');
    Route::post('/loginCheck','Auth\LoginController@loginCheck')->name('memberLogin');
    Route::get('/login/google','Auth\RegisterController@redirectToGoogle')->name('google_login'); 
    Route::get('/google/callback','Auth\RegisterController@handleGoogleCallback')->name('google_callback');
});


Route::group(['middleware' => 'auth'], function(){
    Route::group(['prefix' => 'profile'], function(){
        Route::get('/reg','ProfileController@profileReg')->name('profileReg');
        Route::get('/edit','ProfileController@profileEdit')->name('profileEdit');
        Route::get('/base/edit','ProfileController@profilebaseEdit')->name('profilebaseEdit');
        Route::post('/option/register', 'ProfileController@registerOption')->name('memberOptionRegister');
        Route::post('/option/info', 'ProfileController@updateOption')->name('memberOption');
        // Route::post('/update', 'ProfileController@updateProfile')->name('profileUpdate');
        Route::put('/member/update','ProfileController@profileBaseUpdate')->name('profileUpdate');
    });

    Route::group(['prefix' => 'projects'], function(){
        Route::get('/add', 'ProjectController@newProject')->name('projectAdd');
        Route::post('/create', 'ProjectController@createProject')->name('createProject');
        Route::get('/item/info', 'ProjectController@itemInfo')->name('itemInfo');
        Route::get('/manage/{id}', 'ProjectController@manageProject')->name('projectManage');
        Route::get('/edit/{id}', 'ProjectController@editProject')->name('projectEdit');
        Route::post('/update', 'ProjectController@updateProject')->name('projectUpdate');
    });
    Route::get('/project/{id}','ProjectController@project')->name('project');
    Route::get('/projects','ProjectController@projects')->name('projects');
    Route::get('/dashboard', 'Auth\LoginController@dashboard')->name('dashboard');
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/member','MemberController@memberMain')->name('member');
    Route::get('/dashboardclient','ProjectController@client_main')->name('client');
});

// Route::group(['group' => 'project'],function(){
    // Route::get('/admin','AdminController@adminMain')->name('admin');
    // Route::get('/projects','AdminController@projects')->name('projects');
    // Route::get('/client','ClientController@clientMain')->name('client');
//     Route::get('/add','AdminController@projectAdd')->name('projectAdd');
//     Route::get('/apply','AdminController@projectApply')->name('projectApply');
//     Route::get('/edit','AdminController@projectEdit')->name('projectEdit');
//     Route::get('/manage','AdminController@projectManage')->name('projectManage');
//     Route::get('/','AdminController@project')->name('project');
// });