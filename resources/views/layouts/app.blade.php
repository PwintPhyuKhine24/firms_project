<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="プロジェクトベースで働こう。様々なバックグラウンドをもつプロフェッショナルが少しずつ時間を割き、様々な視点を持ち寄ることで、新しい問題解決のプラットフォームを提供します。firms">
  <link rel="shortcut icon" href="img/favicon.png">
  <title>firm（ファームス）|プロジェクトベースで働く新しいコンサルタントのライフスタイル提案</title>

  <!-- Icons -->
  <link href="{{ asset('vendors/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/css/simple-line-icons.min.css') }}" rel="stylesheet">

  <!-- Main styles for this application -->
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">


</head>

<!-- BODY options, add following classes to body to change options

// Header options
1. '.header-fixed'					- Fixed Header

// Brand options
1. '.brand-minimized'       - Minimized brand (Only symbol)

// Sidebar options
1. '.sidebar-fixed'					- Fixed Sidebar
2. '.sidebar-hidden'				- Hidden Sidebar
3. '.sidebar-off-canvas'		- Off Canvas Sidebar
4. '.sidebar-minimized'			- Minimized Sidebar (Only icons)
5. '.sidebar-compact'			  - Compact Sidebar

// Aside options
1. '.aside-menu-fixed'			- Fixed Aside Menu
2. '.aside-menu-hidden'			- Hidden Aside Menu
3. '.aside-menu-off-canvas'	- Off Canvas Aside Menu

// Breadcrumb options
1. '.breadcrumb-fixed'			- Fixed Breadcrumb

// Footer options
1. '.footer-fixed'					- Fixed footer

-->

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <header class="app-header navbar bg-firms">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand bg-firms" href="#"></a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button">
      <span class="navbar-toggler-icon"></span>
    </button>

    
    @if (Session::has('AUTH'))
    <?php $member = Session::get('AUTH') ?>    

    <ul class="nav navbar-nav ml-auto mr-4">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <img src="{{asset('img/avatars/kao.png')}}" class="img-avatar" alt="mail@zeus.asia">
          <span class="d-md-down-none text-white">{{$member->name}}</span>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          <div class="dropdown-header text-center">
            <strong>アカウント</strong>
          </div>
          <a class="dropdown-item" href="{{route('profileEdit')}}"><i class="fa fa-user"></i> プロファイル編集</a>
          <a class="dropdown-item" href="{{route('logout')}}"><i class="fa fa-lock"></i> ログアウト</a>
        </div>
      </li>
    </ul>

  </header>

  <div class="app-body">
    <div class="sidebar">
      <nav class="sidebar-nav">
        <ul class="nav">
<!-- ここから User Menu -->
          <li class="nav-title">
            Menu
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"><i class="icon-calculator"></i> ダッシュボード</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('projects')}}"><i class="icon-calculator"></i> 案件一覧</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('profileEdit')}}"><i class="icon-calculator"></i> プロファイル編集</a>
          </li>
            @if ($member->role == 1 || $member->role == 99)
            <!-- ここから Client Menu -->
                <li class="divider"></li>
                <li class="nav-title">
                  Client Menu
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{route('itemInfo')}}"><i class="icon-calculator"></i> 案件情報管理</a>
                </li>
              @endif
              @if ($member->role == 99)
              <!-- ここから Admin Menu -->
                <li class="divider"></li>
                <li class="nav-title">
                  Admin Menu
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#"><i class="icon-calculator"></i> 登録者管理</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#"><i class="icon-calculator"></i> クライアント管理</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#"><i class="icon-calculator"></i> 企業情報管理</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#"><i class="icon-calculator"></i> firmマスタ管理</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#"><i class="icon-calculator"></i> termマスタ管理</a>
                </li>

              @endif
          @endif
          

          <li class="divider"></li>
<!-- ここから共通 -->
          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"><i class="icon-calculator"></i> ログアウト</a>
          </li>


        </ul>
      </nav>
      <button class="sidebar-minimizer brand-minimizer" type="button"></button>
    </div>

    <div class="content">
      @yield('content')
    </div>

    {{-- <!-- Main content -->
    <main class="main">

      <!-- Breadcrumb -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">Dashboard</li>
      </ol>

      <div class="container-fluid">
        <div id="ui-view">
          
        </div>
      </div>
      <!-- /.conainer-fluid -->
    </main> --}}

  </div>

  <footer class="app-footer bg-firms text-right">
    <span>Copyright © Shares, Inc. 2020</span>
  </footer>

  <!-- Bootstrap and necessary plugins -->
  <script src="{{ asset('vendors/js/jquery.min.js') }}"></script>
  <script src="{{ asset('vendors/js/popper.min.js')  }}"></script>
  <script src="{{ asset('vendors/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('vendors/js/pace.min.js') }}"></script>

  <!-- Plugins and scripts required by all views -->
  <script src="{{ asset('vendors/js/Chart.min.js') }}"></script>

  <!-- GenesisUI main scripts -->

  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{ asset('js/option.js') }}"></script>

</body>
</html>
