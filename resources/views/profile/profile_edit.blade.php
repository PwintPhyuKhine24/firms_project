@extends('layouts/app')
@section('content')
    <main class="main">

        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item active">プロファイル編集</li>
        </ol>

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card p-5">
                        <h3><i class="fa fa-user"></i> ファームス太郎様 プロファイル編集</h3>
                        <table class="table table-striped mt-3">
                            <tr>
                                <th class="bg-firms">ファーム歴</th>
                                <td>
                                    <table class="table table-bordered">
                                        <tr>
                                            <th class="bg-firms">ファーム名</th>
                                            <th class="bg-firms">在籍期間</th>
                                        </tr>
                                        @isset($firm_term01)
                                            <tr>
                                              <td>{{ $firm_term01->firm }}</td>
                                              <td>{{ $firm_term01->term }}</td>
                                            </tr>
                                        @endisset
                                        @isset($firm_term02)
                                            <tr>
                                              <td>{{ $firm_term02->firm }}</td>
                                              <td>{{ $firm_term02->term }}</td>
                                            </tr>
                                        @endisset
                                        @isset($firm_term03)
                                            <tr>
                                              <td>{{ $firm_term03->firm }}</td>
                                              <td>{{ $firm_term03->term }}</td>
                                            </tr>
                                        @endisset
                                        @isset($firm_term04)
                                            <tr>
                                              <td>{{ $firm_term04->firm }}</td>
                                              <td>{{ $firm_term04->term }}</td>
                                            </tr>
                                        @endisset
                                        @isset($firm_term05)
                                            <tr>
                                              <td>{{ $firm_term05->firm }}</td>
                                              <td>{{ $firm_term05->term }}</td>
                                            </tr>
                                        @endisset
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <th class="bg-firms">登録メールアドレス</th>
                                <td>
                                    <a href="mailto:{{ $member_mail }}">{{ $member_mail }}</a>
                                </td>
                            </tr>
                        </table>

                        <button class="btn btn-lg btn-firms"><a href="{{ route('profilebaseEdit') }}"
                                class="text-white">基礎情報を編集する</a></button>

                    </div>
                    <div class="card p-5">

                      <form action="{{ route('memberOption') }}" method="post">
                        @csrf               
                        <div class="my-2">
                            <h4>今まで経験した業務内容をご記入ください（複数回答可）</h4>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th class="bg-firms" width="25%">事業戦略</th>
                                        <th class="bg-firms" width="25%">グローバル戦略</th>
                                        <th class="bg-firms" width="25%">マーケティング</th>
                                        <th class="bg-firms" width="25%">収益改善<br />（バリューアップ）</th>
                                    </tr>
                                </thead>
                                  <tr>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="a_01"
                                            id="a_01" />市場環境分析</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="b_01"
                                            id="b_01" />中国</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="c_01"
                                            id="c_01" />顧客・製品分析</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="d_01"
                                            id="d_01" />売上向上機会の分析</td>
                                </tr>
                                <tr>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="a_02"
                                            id="a_02" />戦略策定</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="b_02"
                                            id="b_02" />アジア</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="c_02"
                                            id="c_02" />ターゲティング<br />・ポジショニング</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="d_02"
                                            id="d_02" />コスト削減機会の分析</td>
                                </tr>
                                <tr>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="a_03"
                                            id="a_03" />実行サポート</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="b_03"
                                            id="b_03" />欧州</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="c_03"
                                            id="c_03" />実行サポート</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="d_03"
                                            id="d_03" />実行サポート</td>
                                </tr>
                            </table>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th class="bg-firms" width="25%">M&A</th>
                                        <th class="bg-firms" width="25%">経営管理体制の確立</th>
                                        <th class="bg-firms" colspan=2 width="50%">その他</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="e_01"
                                            id="e_01" />M&A戦略</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="f_01"
                                            id="f_01" />管理会計・KPI設計</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="g_01"
                                            id="g_01" />システム構築（要件定義、構築実務）</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="g_04"
                                            id="g_04" />トレーニング・研修講師</td>
                                </tr>
                                <tr>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="e_02"
                                            id="e_02" />デューデリジェンス、企業価値評価</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="f_02"
                                            id="f_02" />人事制度構築</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="g_02"
                                            id="g_02" />エグゼクティブコーチング</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="g_05"
                                            id="g_05" />プロジェクトアシスタント<br />（議事録取り、分析作業、連絡、事務局係）</td>
                                </tr>
                                <tr>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="e_03"
                                            id="e_03" />統合プロセス</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="f_03"
                                            id="f_03" />組織設計</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="g_03"
                                            id="g_03" />改革のファシリテーション</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="g_06"
                                            id="g_06" />その他</td>
                                </tr>
                            </table>
                          </div>
                          
                        <br />
                        <div class="my-2">
                            <h4>学歴</h4>
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th class="bg-firms"><label for="member_大学">大学</label></th>
                                    <th class="bg-firms"><label for="member_学部">学部</label></th>
                                </tr>
                                <tr>
                                    <td><input class="form-control" type="text" value="{{ $member_option->university ? $member_option->university : '' }}" name="member_university"
                                            id="member_university" /></td>
                                    <td><input class="form-control" type="text" value="{{ $member_option->faculty ? $member_option->faculty : '' }}" name="member_faculty"
                                            id="member_faculty" /></td>
                                </tr>
                                <tr>
                                    <th class="bg-firms"><label for="member_大学院">大学院</label></th>
                                    <th class="bg-firms"><label for="member_専攻">専攻</label></th>
                                </tr>
                                <tr>
                                    <td><input class="form-control" type="text" value="{{ $member_option->graduate ? $member_option->graduate : '' }}" name="member_graduate"
                                            id="member_graduate" /></td>
                                    <td><input class="form-control" type="text" value="{{ $member_option->speciality ? $member_option->speciality : '' }}" name="member_speciality"
                                            id="member_speciality" /></td>
                                </tr>
                            </table>
                        </div>
                        <div class="my-2" id="formtype_05">
                            <h4><label for="member_電話番号（ハイフンは入れずにご入力下さい。）">電話番号（ハイフンは入れずにご入力下さい。）</label></h4>
                            <input class="form-control" type="text" value="{{ $member_option->telephone ? $member_option->telephone : '' }}" name="member_telephone"
                                id="member_telephone" />
                        </div>
                        <br />
                        <div class="row my-2" id="formtype_07">
                            <div class="col-sm-6">
                                <h4><label for="member_facebook ID">Facebook id</label></h4>
                                <input class="form-control" type="text" value="{{ $member_option->facebook_id ? $member_option->facebook_id : '' }}" name="member_facebookURL"
                                    id="member_facebookURL" />
                            </div>

                            <div class="col-sm-6">
                                <h4><label for="member_linkedin ID">Linkedin id</label></h4>
                                <input class="form-control" type="text" value="{{ $member_option->linkedin_id ? $member_option->linkedin_id : '' }}" name="member_linkedinURL"
                                    id="member_linkedinURL" />
                            </div>
                        </div>
                        <br />
                        <div class="my-2" id="formtype_11">
                            <h4><label for="member_案件希望条件">案件希望条件</label></h4>
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <td>
                                        <input name="member[wish][]" type="hidden" value="" /><input name="member_wish[]"
                                            type="checkbox" value="w_01" id="w_01" />上場企業
                                    </td>
                                    <td>
                                        <input name="member[wish][]" type="hidden" value="" /><input name="member_wish[]"
                                            type="checkbox" value="w_02" id="w_02" />ベンチャー
                                    </td>
                                    <td>
                                        <input name="member[wish][]" type="hidden" value="" /><input name="member_wish[]"
                                            type="checkbox" value="w_03" id="w_03" />ＮＰＯ／ＮＧＯ
                                    </td>
                                    <td>
                                        <input name="member[wish][]" type="hidden" value="" /><input name="member_wish[]"
                                            type="checkbox" value="w_04" id="w_04" />クライアント担当者が経営層
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="member[wish][]" type="hidden" value="" /><input name="member_wish[]"
                                            type="checkbox" value="w_05" id="w_05" />フィーサイズ　500万円以上
                                    </td>
                                    <td>
                                        <input name="member[wish][]" type="hidden" value="" /><input name="member_wish[]"
                                            type="checkbox" value="w_06" id="w_06" />社会的意義が強い
                                    </td>
                                    <td>
                                        <input name="member[wish][]" type="hidden" value="" /><input name="member_wish[]"
                                            type="checkbox" value="w_07" id="w_07" />継続性が高い
                                    </td>
                                    <td>
                                        <input name="member[wish][]" type="hidden" value="" /><input name="member_wish[]"
                                            type="checkbox" value="w_08" id="w_08" />その他
                                </tr>
                            </table>
                        </div>

                        <div class="row my-2" id="formtype_19">
                            <div class="col-sm-6">
                                <h4><label for="member_希望時間単価">希望時間単価</label></h4>
                                <div class="input-group">
                                    <select class="form-control" name="member_unitprice" id="member_unitprice">
                                        <option value="1000" {{ $member_option->unitprice == '1000' ? 'selected' : '' }}>1,000</option>
                                        <option value="2500" {{ $member_option->unitprice == '2500' ? 'selected' : '' }}>2,500</option>
                                        <option value="4000" {{ $member_option->unitprice == '4000' ? 'selected' : '' }}>4,000</option>
                                        <option value="5000" {{ $member_option->unitprice == '5000' ? 'selected' : '' }}>5,000</option>
                                        <option value="6000" {{ $member_option->unitprice == '6000' ? 'selected' : '' }}>6,000</option>
                                        <option value="8000" {{ $member_option->unitprice == '8000' ? 'selected' : '' }}>8,000</option>
                                        <option value="10000" {{ $member_option->unitprice == '10000' ? 'selected' : '' }}>10,000</option>
                                        <option value="12000" {{ $member_option->unitprice == '12000' ? 'selected' : '' }}>12,000</option>
                                        <option value="15000" {{ $member_option->unitprice == '15000' ? 'selected' : '' }}>15,000</option>
                                    </select>
                                    <span class="input-group-addon">円／時間</span>
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <h4><label for="member_アベイラビリティ（稼働可能時間）">アベイラビリティ（稼働可能時間）</label></h4>
                                <div class="input-group">
                                    <span class="input-group-addon">週</span>
                                    <select class="form-control" name="member_availability" id="member_availability">
                                        <option value="1" {{ $member_option->availability == '1' ? 'selected' : '' }}>1</option>
                                        <option value="2" {{ $member_option->availability == '2' ? 'selected' : '' }}>2</option>
                                        <option value="3" {{ $member_option->availability == '3' ? 'selected' : '' }}>3</option>
                                        <option value="4" {{ $member_option->availability == '4' ? 'selected' : '' }}>4</option>
                                        <option value="5" {{ $member_option->availability == '5' ? 'selected' : '' }}>5</option>
                                    </select>
                                    <span class="input-group-addon">日</span>
                                </div>

                            </div>
                        </div>


                        <div class="my-2" id="formtype_20">
                            <h4>その他自由記載欄</h4>
                            <textarea class="form-control" name="member_remark">{{ $member_option->remark ? $member_option->remark : '' }}</textarea>
                        </div>


                        <br class="clear" />
                        <div class="my-2">
                            <span id="lbl_pji"><br /></span>
                        </div>

                        <button type="submit" class="btn btn-lg btn-firms text-white w-100">更新する</button>

                      </form>

                    </div>

                </div>

            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

    <script>     
      var members = {!! json_encode($member_option->toArray()) !!};
      var exp = members['experience'];    
      var wish = members['wish'];
      var expData = exp.split(',');
      var wishData = wish.split(',');
      for(i = 0; i < expData.length; i++)
      {
        switch(expData[i])
        {
          case 'a_01':
            document.getElementById('a_01').checked = true;
            break;
          case 'b_01':
            document.getElementById('b_01').checked = true;
            break;
          case 'c_01':
            document.getElementById('c_01').checked = true;
            break;
          case 'd_01':
            document.getElementById('d_01').checked = true;
            break;

          case 'a_02':
            document.getElementById('a_02').checked = true;
            break;
          case 'b_02':
            document.getElementById('b_02').checked = true;
            break;
          case 'c_02':
            document.getElementById('c_02').checked = true;
            break;
          case 'd_02':
            document.getElementById('d_02').checked = true;
            break;
          
          case 'a_03':
            document.getElementById('a_03').checked = true;
            break;
          case 'b_03':
            document.getElementById('b_03').checked = true;
            break;
          case 'c_03':
            document.getElementById('c_03').checked = true;
            break;
          case 'd_03':
            document.getElementById('d_03').checked = true;
            break;
          
          case 'e_01':
            document.getElementById('e_01').checked = true;
            break;
          case 'f_01':
            document.getElementById('f_01').checked = true;
            break;
          case 'g_01':
            document.getElementById('g_01').checked = true;
            break;
          case 'g_04':
            document.getElementById('g_04').checked = true;
            break;

          case 'e_02':
            document.getElementById('e_02').checked = true;
            break;
          case 'f_02':
            document.getElementById('f_02').checked = true;
            break;
          case 'g_02':
            document.getElementById('g_02').checked = true;
            break;
          case 'g_05':
            document.getElementById('g_05').checked = true;
            break;
          
          case 'e_03':
            document.getElementById('e_03').checked = true;
            break;
          case 'f_03':
            document.getElementById('f_03').checked = true;
            break;
          case 'g_03':
            document.getElementById('g_03').checked = true;
            break;
          case 'g_06':
            document.getElementById('g_06').checked = true;
            break;
          default:
            break;
        }
        console.log(expData[i])
      } 
      for(j = 0; j<wishData.length; j++)
      {
        switch (wishData[j]) {
          case 'w_01':
            document.getElementById('w_01').checked = true;
            break;
          case 'w_02':
            document.getElementById('w_02').checked = true;
            break;
          case 'w_03':
            document.getElementById('w_03').checked = true;
            break;
          case 'w_04':
            document.getElementById('w_04').checked = true;
            break;
          case 'w_05':
            document.getElementById('w_05').checked = true;
            break;
          case 'w_06':
            document.getElementById('w_06').checked = true;
            break;
          case 'w_07':
            document.getElementById('w_07').checked = true;
            break;
          case 'w_08':
            document.getElementById('w_08').checked = true;
            break;
          default:
            break;
        }
      }   
    </script>
@endsection
