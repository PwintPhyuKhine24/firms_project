@extends('layouts/app')
@section('content')

<!-- Main content -->
<main class="main">

<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">プロファイル編集</li>
<li class="breadcrumb-item active">プロファイル基礎情報編集</li>
</ol>
<div class="container-fluid">
<div class="row">
<div class="col-12">
<form action="{{ route('profileUpdate') }}" method="post">
@csrf
@method('PUT')
<div class="card p-5">
<h3><i class="fa fa-user"></i> ファームス太郎様 プロファイル基礎情報編集</h3>
<table class="table table-striped mt-3">
    <tr>
        <th class="bg-firms">ファーム歴</th>
        <td>
            <table class="table table-sm table-responsive">
                <tr id="tr_firm1">
                    <th><label for="member_ファーム名">ファーム名</label></th>
                    <th><label for="member_在籍期間">在籍期間</label></th>
                </tr>
                @isset($firm_term01)
                <tr>
                    <td>
                      <select class="form-control" name="member_firm1" id="member_firm1">
                            @foreach ($firms as $row)
                                <option value="{{ $row->id }}" {{ $member_option->firm_id01 == $row->id ? 'selected' : ''}}>{{ $row->firm }}</option>
                            @endforeach
                      </select>
                    </td>
                    <td>
                      <select class="form-control" name="member_term1" id="member_term1">
                            
                          @foreach ($terms as $rows)
                              <option value="{{ $rows->id }}" {{ $member_option->term_id01 == $rows->id ? 'selected' : ''}}>{{ $rows->term }}</option>
                          @endforeach
                      </select>
                    </td>
                </tr>
                @endisset
                
                {{-- @for ($i = 1; $i < 6; $i++)
                  <tr>
                      <td>
                        <select class="form-control" name="member[firm1]" id="member_firm1">
                          @foreach ($firms as $firm)
                            <option value="{{ $firm->id }}" {{ $member_option->firm_id01 == $firm->id ? 'selected' : ''}}>{{ $firm->firm }}</option>
                          @endforeach
                        </select>
                      </td>
                      <td>
                        <select class="form-control" name="member[term1]" id="member_term1">
                          @foreach ($terms as $term)
                            <option value="{{ $term->id }}" {{ $member_option->term_id01 == $term->id ? 'selected' : ''}}>{{ $term->term }}</option>
                          @endforeach
                        </select>
                      </td>
                    
                  </tr>
                @endfor --}}
                @isset($firm_term02)
                <tr id="tr_firm2">
                    <td><select class="form-control" name="member_firm2" id="member_firm2">
                            @foreach ($firms as $row)
                                <option value="{{ $row->id }}" {{ $member_option->firm_id02 == $row->id ? 'selected' : ''}} >{{ $row->firm }}</option>
                            @endforeach
                        </select></td>
                    <td><select class="form-control" name="member_term2" id="member_term2">
                            
                            @foreach ($terms as $rows)
                                <option value="{{ $rows->id }}" {{ $member_option->term_id02 == $rows->id ? 'selected' : ''}}>{{ $rows->term }}</option>
                            @endforeach
                        </select></td>
                </tr>
                @endisset

                @isset($firm_term03)
                <tr id="tr_firm3">
                    <td>
                      <select class="form-control" name="member_firm3" id="member_firm3">
                            @foreach ($firms as $row)
                                <option value="{{ $row->id }}" {{ $member_option->firm_id03 == $row->id ? 'selected' : ''}}>{{ $row->firm }}</option>
                            @endforeach
                        </select>
                      </td>
                    <td>
                      <select class="form-control" name="member_term3" id="member_term3">
                          @foreach ($terms as $rows)
                              <option value="{{ $rows->id }}" {{ $member_option->term_id03 == $rows->id ? 'selected' : ''}}>{{ $rows->term }}</option>
                          @endforeach
                      </select>
                    </td>
                </tr>
                @endisset

                @isset($firm_term04)
                <tr id="tr_firm4" >
                    <td>
                      <select class="form-control" name="member_firm4" id="member_firm4">
                            @foreach ($firms as $row)
                                <option value="{{ $row->id }}" {{ $member_option->firm_id04 == $row->id ? 'selected' : ''}}>{{ $row->firm }}</option>
                            @endforeach
                        </select>
                      </td>
                    <td>
                      <select class="form-control" name="member_term4" id="member_term4">
                            
                          @foreach ($terms as $rows)
                              <option value="{{ $rows->id }}" {{ $member_option->term_id04 == $rows->id ? 'selected' : ''}}>{{ $rows->term }}</option>
                          @endforeach
                      </select>
                    </td>
                </tr>
                @endisset

                @isset($firm_term05)
                <tr id="tr_firm5">
                    <td>
                      <select class="form-control" name="member_firm5" id="member_firm5">
                            @foreach ($firms as $row)
                                <option value="{{ $row->id }}" {{ $member_option->firm_id05 == $row->id ? 'selected' : ''}}>{{ $row->firm }}</option>
                            @endforeach
                        </select>
                      </td>
                    <td>
                      <select class="form-control" name="member_term5" id="member_term5">
                            
                          @foreach ($terms as $rows)
                              <option value="{{ $rows->id }}" {{ $member_option->term_id05 == $rows->id ? 'selected' : ''}}>{{ $rows->term }}</option>
                          @endforeach
                      </select>
                    </td>
                </tr>
                @endisset

                {{-- <tr id="tr_firm2" style="display: none">
                  <td>
                    <select class="form-control" name="member_firm2" id="member_firm4">
                          @foreach ($firms as $row)
                              <option value="{{ $row->id }}" {{ $member_option->firm_id04 == $row->id ? 'selected' : ''}}>{{ $row->firm }}</option>
                          @endforeach
                      </select>
                    </td>
                  <td>
                    <select class="form-control" name="member_term2" id="member_term4">
                          
                        @foreach ($terms as $rows)
                            <option value="{{ $rows->id }}" {{ $member_option->term_id04 == $row->id ? 'selected' : ''}}>{{ $rows->term }}</option>
                        @endforeach
                    </select>
                  </td>
                </tr>
                <tr id="tr_firm3" style="display: none">
                  <td>
                    <select class="form-control" name="member[firm4]" id="member_firm4">
                          @foreach ($firms as $row)
                              <option value="{{ $row->id }}" {{ $member_option->firm_id04 == $row->id ? 'selected' : ''}}>{{ $row->firm }}</option>
                          @endforeach
                      </select>
                    </td>
                  <td>
                    <select class="form-control" name="member[term4]" id="member_term4">
                          
                        @foreach ($terms as $rows)
                            <option value="{{ $rows->id }}" {{ $member_option->term_id04 == $row->id ? 'selected' : ''}}>{{ $rows->term }}</option>
                        @endforeach
                    </select>
                  </td>
                </tr>
                <tr id="tr_firm4" style="display: none">
                  <td>
                    <select class="form-control" name="member[firm4]" id="member_firm4">
                          @foreach ($firms as $row)
                              <option value="{{ $row->id }}" {{ $member_option->firm_id04 == $row->id ? 'selected' : ''}}>{{ $row->firm }}</option>
                          @endforeach
                      </select>
                    </td>
                  <td>
                    <select class="form-control" name="member[term4]" id="member_term4">
                          
                        @foreach ($terms as $rows)
                            <option value="{{ $rows->id }}" {{ $member_option->term_id04 == $row->id ? 'selected' : ''}}>{{ $rows->term }}</option>
                        @endforeach
                    </select>
                  </td>
                </tr>
                <tr id="tr_firm5" style="display: none">
                  <td>
                    <select class="form-control" name="member[firm4]" id="member_firm4">
                          @foreach ($firms as $row)
                              <option value="{{ $row->id }}" {{ $member_option->firm_id04 == $row->id ? 'selected' : ''}}>{{ $row->firm }}</option>
                          @endforeach
                      </select>
                    </td>
                  <td>
                    <select class="form-control" name="member[term4]" id="member_term4">
                          
                        @foreach ($terms as $rows)
                            <option value="{{ $rows->id }}" {{ $member_option->term_id04 == $row->id ? 'selected' : ''}}>{{ $rows->term }}</option>
                        @endforeach
                    </select>
                  </td>
                </tr> --}}

                <tr id="tr_firm2" style="display: none;">
                  <td><select class="form-control" name="member_firm2" id="member_firm2">
                    <option value="">-</option>
                    @foreach ($firms as $row)
                      <option value="{{$row->id}}" name="member_firm2">{{$row->firm}}</option>
                    @endforeach
                    </select></td>
                  <td><select class="form-control" name="member_term2" id="member_term2">
                    <option value="">-</option>
                    @foreach ($terms as $rows)
                      <option value="{{$rows->id}}" name="member_term2">{{$rows->term}}</option> 
                    @endforeach
                    </select></td>
                </tr>
                <tr id="tr_firm3" style="display: none;">
                  <td><select class="form-control" name="member_firm3" id="member_firm3">
                    <option value="">-</option>
                    @foreach ($firms as $row)
                      <option value="{{$row->id}}" name="member_firm3">{{$row->firm}}</option>
                    @endforeach
                    </select></td>
                  <td><select class="form-control" name="member_term3" id="member_term3">
                      <option value="">-</option>
                      @foreach ($terms as $rows)
                        <option value="{{$rows->id}}" name="member_term3">{{$rows->term}}</option> 
                      @endforeach
                    </select></td>
                </tr>
                <tr id="tr_firm4" style="display: none;">
                  <td><select class="form-control" name="member_firm4" id="member_firm4">
                    <option value="">-</option>
                    @foreach ($firms as $row)
                      <option value="{{$row->id}}" name="member_firm4">{{$row->firm}}</option>
                    @endforeach
                    </select></td>
                  <td><select class="form-control" name="member_term4" id="member_term4">
                      <option value="">-</option>
                      @foreach ($terms as $rows)
                        <option value="{{$rows->id}}" name="member_term4">{{$rows->term}}</option> 
                      @endforeach
                    </select></td>
                </tr>
                <tr id="tr_firm5" style="display: none;">
                  <td><select class="form-control" name="member_firm5" id="member_firm5">
                    <option value="">-</option>
                    @foreach ($firms as $row)
                    <option value="{{$row->id}}" name="member_firm5">{{$row->firm}}</option>
                  @endforeach
                    </select></td>
                  <td><select class="form-control" name="member_term5" id="member_term5">
                      <option value="">-</option>
                      @foreach ($terms as $rows)
                        <option value="{{$rows->id}}" name="member_term5">{{$rows->term}}</option> 
                      @endforeach
                    </select></td>
                </tr>
                
            </table>
            <div class="ri mb-4">
                <input type="button" class="btn btn-primary firm_add_edit" value="ファーム歴追加(5つまで)"
                    id="firm_add_edit">
            </div>
            <!--
<div class="ri">
<span id="lbl_base_pji" class="my-4"></span><br/>
<input type="button" class="btn btn-primary" value="あなたの実績から想定される年収を見る" id="base_pji_update" >
</div>
-->
        </td>
    </tr>
    <tr>
        <th class="bg-firms">お名前</th>
        <td>
            <input class="form-control" type="text" value="{{ $members->name }}"
                name="member_name" id="member_name" />
        </td>
    </tr>
    <tr>
        <th class="bg-firms">登録メールアドレス</th>
        <td>
            <input class="form-control" type="text" value="{{ $members->email }}"
                name="member_mail" id="member_mail" />
        </td>
    </tr>
    <tr>
        <th class="bg-firms">パスワード</th>
        <td>
            <div class="input-group">
                <span class="input-group-addon">修正用</span>
                <input class="form-control" type="password" name="member_pass"
                    id="member_pass" />
                {{-- <input type="hidden"
                    value="{{ $members->password }}" name="old_member_pass">
                --}}
            </div>
            <div class="input-group">
                <span class="input-group-addon">確認用</span>
                <input class="form-control" type="password" name="member_pass_c"
                    id="member_pass_c" />
            </div>
            上下に同じものを入力してください。
        </td>
    </tr>
</table>

<button type="submit" class="btn btn-lg btn-firms">基礎情報を更新する</button>

</div>
</form>

</div>

</div>
</div>
<!-- /.conainer-fluid -->
</main>

</div>


@endsection
