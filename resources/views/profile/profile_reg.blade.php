@extends('layouts/app')
@section('content')

    <!-- Main content -->
    <main class="main">

        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item active">プロファイル登録</li>
        </ol>

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card p-5">
                        <h3><i class="fa fa-user"></i> ファームス太郎様 プロファイル登録</h3>
                        <table class="table table-striped mt-3">
                            <tr>
                                <th>ファーム歴</th>
                                <td>
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>ファーム名</th>
                                            <th>在籍期間</th>
                                        </tr>
                                        @isset($firm_term01)
                                            <tr>
                                              <td>{{ $firm_term01->firm }}</td>
                                              <td>{{ $firm_term01->term }}</td>
                                            </tr>
                                        @endisset
                                        @isset($firm_term02)
                                            <tr>
                                              <td>{{ $firm_term02->firm }}</td>
                                              <td>{{ $firm_term02->term }}</td>
                                            </tr>
                                        @endisset
                                        @isset($firm_term03)
                                            <tr>
                                              <td>{{ $firm_term03->firm }}</td>
                                              <td>{{ $firm_term03->term }}</td>
                                            </tr>
                                        @endisset
                                        @isset($firm_term04)
                                            <tr>
                                              <td>{{ $firm_term04->firm }}</td>
                                              <td>{{ $firm_term04->term }}</td>
                                            </tr>
                                        @endisset
                                        @isset($firm_term05)
                                            <tr>
                                              <td>{{ $firm_term05->firm }}</td>
                                              <td>{{ $firm_term05->term }}</td>
                                            </tr>
                                        @endisset
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <th>登録メールアドレス</th>
                                <td>
                                    <a href="mailto:{{ $member_mail }}">{{ $member_mail }}</a>
                                </td>
                            </tr>
                        </table>

                        ※プロファイル情報登録完了後、お名前、ファーム歴、登録メールアドレス等につきましても修正可能です。

                    </div>
                    <div class="card p-5">

                        <form action="{{ route('memberOptionRegister') }}" method="post">
                        @csrf
                        
                        <div class="my-2">
                            <h4>今まで経験した業務内容をご記入ください（複数回答可）</h4>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th class="bg-firms" width="25%">事業戦略</th>
                                        <th class="bg-firms" width="25%">グローバル戦略</th>
                                        <th class="bg-firms" width="25%">マーケティング</th>
                                        <th class="bg-firms" width="25%">収益改善<br />（バリューアップ）</th>
                                    </tr>
                                </thead>
                                  <tr>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="a_01"
                                            id="member_experience" />市場環境分析</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="b_01"
                                            id="member_experience" />中国</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="c_01"
                                            id="member_experience" />顧客・製品分析</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="d_01"
                                            id="member_experience" />売上向上機会の分析</td>
                                </tr>
                                <tr>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="a_02"
                                            id="member_experience" />戦略策定</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="b_02"
                                            id="member_experience" />アジア</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="c_02"
                                            id="member_experience" />ターゲティング<br />・ポジショニング</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="d_02"
                                            id="member_experience" />コスト削減機会の分析</td>
                                </tr>
                                <tr>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="a_03"
                                            id="member_experience" />実行サポート</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="b_03"
                                            id="member_experience" />欧州</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="c_03"
                                            id="member_experience" />実行サポート</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="d_03"
                                            id="member_experience" />実行サポート</td>
                                </tr>
                            </table>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th class="bg-firms" width="25%">M&A</th>
                                        <th class="bg-firms" width="25%">経営管理体制の確立</th>
                                        <th class="bg-firms" colspan=2 width="50%">その他</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="e_01"
                                            id="member_experience" />M&A戦略</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="f_01"
                                            id="member_experience" />管理会計・KPI設計</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="g_01"
                                            id="member_experience" />システム構築（要件定義、構築実務）</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="g_04"
                                            id="member_experience" />トレーニング・研修講師</td>
                                </tr>
                                <tr>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="e_02"
                                            id="member_experience" />デューデリジェンス、企業価値評価</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="f_02"
                                            id="member_experience" />人事制度構築</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="g_02"
                                            id="member_experience" />エグゼクティブコーチング</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="g_05"
                                            id="member_experience" />プロジェクトアシスタント<br />（議事録取り、分析作業、連絡、事務局係）</td>
                                </tr>
                                <tr>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="e_03"
                                            id="member_experience" />統合プロセス</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="f_03"
                                            id="member_experience" />組織設計</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="g_03"
                                            id="member_experience" />改革のファシリテーション</td>
                                    <td><input name="member[experience][]" type="hidden" value="" /><input
                                            name="member_experience[]" type="checkbox" value="g_06"
                                            id="member_experience" />その他</td>
                                </tr>
                            </table>
                          </div>
                          
                        <br />
                        <div class="my-2">
                            <h4>学歴</h4>
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th class="bg-firms"><label for="member_大学">大学</label></th>
                                    <th class="bg-firms"><label for="member_学部">学部</label></th>
                                </tr>
                                <tr>
                                    <td><input class="form-control" type="text" value="" name="member_university"
                                            id="member_university" /></td>
                                    <td><input class="form-control" type="text" value="" name="member_faculty"
                                            id="member_faculty" /></td>
                                </tr>
                                <tr>
                                    <th class="bg-firms"><label for="member_大学院">大学院</label></th>
                                    <th class="bg-firms"><label for="member_専攻">専攻</label></th>
                                </tr>
                                <tr>
                                    <td><input class="form-control" type="text" value="" name="member_graduate"
                                            id="member_graduate" /></td>
                                    <td><input class="form-control" type="text" value="" name="member_speciality"
                                            id="member_speciality" /></td>
                                </tr>
                            </table>
                        </div>
                        <div class="my-2" id="formtype_05">
                            <h4><label for="member_電話番号（ハイフンは入れずにご入力下さい。）">電話番号（ハイフンは入れずにご入力下さい。）</label></h4>
                            <input class="form-control" type="text" value="" name="member_telephone"
                                id="member_telephone" />
                        </div>
                        <br />
                        <div class="row my-2" id="formtype_07">
                            <div class="col-sm-6">
                                <h4><label for="member_facebook ID">Facebook id</label></h4>
                                <input class="form-control" type="text" value="" name="member_facebookURL"
                                    id="member_facebookURL" />
                            </div>
                            <div class="col-sm-6">
                                <h4><label for="member_linkedin ID">Linkedin id</label></h4>
                                <input class="form-control" type="text" value="" name="member_linkedinURL"
                                    id="member_linkedinURL" />
                            </div>
                        </div>
                        <br />
                        <div class="my-2" id="formtype_11">
                            <h4><label for="member_案件希望条件">案件希望条件</label></h4>

                            <table class="table table-striped table-bordered">
                                <tr>
                                    <td>
                                        <input name="member[wish][]" type="hidden" value="" /><input name="member_wish[]"
                                            type="checkbox" value="w_01" id="member_wish" />上場企業
                                    </td>
                                    <td>
                                        <input name="member[wish][]" type="hidden" value="" /><input name="member_wish[]"
                                            type="checkbox" value="w_02" id="member_wish" />ベンチャー
                                    </td>
                                    <td>
                                        <input name="member[wish][]" type="hidden" value="" /><input name="member_wish[]"
                                            type="checkbox" value="w_03" id="member_wish" />ＮＰＯ／ＮＧＯ
                                    </td>
                                    <td>
                                        <input name="member[wish][]" type="hidden" value="" /><input name="member_wish[]"
                                            type="checkbox" value="w_04" id="member_wish" />クライアント担当者が経営層
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="member[wish][]" type="hidden" value="" /><input name="member_wish[]"
                                            type="checkbox" value="w_05" id="member_wish" />フィーサイズ　500万円以上
                                    </td>
                                    <td>
                                        <input name="member[wish][]" type="hidden" value="" /><input name="member_wish[]"
                                            type="checkbox" value="w_06" id="member_wish" />社会的意義が強い
                                    </td>
                                    <td>
                                        <input name="member[wish][]" type="hidden" value="" /><input name="member_wish[]"
                                            type="checkbox" value="w_07" id="member_wish" />継続性が高い
                                    </td>
                                    <td>
                                        <input name="member[wish][]" type="hidden" value="" /><input name="member_wish[]"
                                            type="checkbox" value="w_08" id="member_wish" />その他
                                </tr>
                            </table>
                        </div>

                        <div class="row my-2" id="formtype_19">
                            <div class="col-sm-6">
                                <h4><label for="member_希望時間単価">希望時間単価</label></h4>
                                <div class="input-group">
                                    <select class="form-control" name="member_unitprice" id="member_unitprice">
                                        <option value="1000">1,000</option>
                                        <option value="2500">2,500</option>
                                        <option value="4000">4,000</option>
                                        <option value="5000">5,000</option>
                                        <option value="6000">6,000</option>
                                        <option value="8000">8,000</option>
                                        <option value="10000">10,000</option>
                                        <option value="12000">12,000</option>
                                        <option value="15000">15,000</option>
                                    </select>
                                    <span class="input-group-addon">円／時間</span>
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <h4><label for="member_アベイラビリティ（稼働可能時間）">アベイラビリティ（稼働可能時間）</label></h4>
                                <div class="input-group">
                                    <span class="input-group-addon">週</span>
                                    <select class="form-control" name="member_availability" id="member_availability">
                                        <option value="1" >1</option>
                                        <option value="2" >2</option>
                                        <option value="3" >3</option>
                                        <option value="4" >4</option>
                                        <option value="5" >5</option>
                                    </select>
                                    <span class="input-group-addon">日</span>
                                </div>

                            </div>
                        </div>


                        <div class="my-2" id="formtype_20">
                            <h4>その他自由記載欄</h4>
                            <textarea class="form-control" name="member_remark"></textarea>
                        </div>


                        <br class="clear" />
                        <div class="my-2">
                            <span id="lbl_pji"><br /></span>
                        </div>

                        <button type="submit" class="btn btn-lg btn-firms text-white w-100">登録</button>

                      </form>
                    </div>

                </div>

            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

    </div>
@endsection
