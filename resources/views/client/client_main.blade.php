@extends('layouts/app')
@section('content')

    <!-- Main content -->
    <main class="main">

        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item active">案件情報管理</li>
        </ol>

        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card p-5 w-100">
                        <h4>案件情報管理</h4>
                        <table class="table table-responsive table-bordered table-hovered">
                            <tr class="bg-firms">
                                <th>案件名</th>
                                <th>クライアント名</th>
                                <th>案件ステータス</th>
                                <th>登録日</th>
                                <th>最終更新日</th>
                                <th>応募状況</th>
                                <th>応募者管理</th>
                                <th>案件編集</th>
                            </tr>
                            @isset($projects)
                            @foreach ($projects as $project)
                              <tr>
                                  <td>{{ $project->term }}</td>
                                  <td>{{ $project->client_name }}</td>
                                  <td>{{ $project->status }}</td>
                                  <td>{{ $project->created_at }}</td>
                                  <td>
                                    @if ($project->updated_at == '')
                                        {{ $project->created_at }}
                                    @else
                                        {{ $project->updated_at }}
                                    @endif
                                  </td>
                                  <td>2名</td>
                                  <td><a href="{{ url('projects/manage', $project->id) }}" class="btn btn-primary mb-2">閲覧</a></td>
                                  <td><a href="{{ url('projects/edit', $project->id) }}" class="btn btn-primary mb-2">編集</a></td>
                              </tr>                               
                            @endforeach
                            @endisset
                        </table>

                        <button class="btn btn-lg btn-firms"><a href="{{ route('projectAdd') }}"
                                class="text-white">新たに案件を登録する</a></button>


                    </div>
                </div>
            </div>

        </div>
        <!-- /.conainer-fluid -->
    </main>

    </div>
@endsection
