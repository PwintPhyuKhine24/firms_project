<!DOCTYPE html>
<html lang="ja">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description"
    content="プロジェクトベースで働こう。様々なバックグラウンドをもつプロフェッショナルが少しずつ時間を割き、様々な視点を持ち寄ることで、新しい問題解決のプラットフォームを提供します。firms">
  <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
  
  <title>firm（ファームス）|プロジェクトベースで働く新しいコンサルタントのライフスタイル提案</title>

  <!-- Icons -->
  <link href="{{ asset('vendors/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/css/simple-line-icons.min.css') }}" rel="stylesheet">

  <!-- Main styles for this application -->
  <link href="{{ asset('css/style.css')}}" rel="stylesheet">

</head>

<body>
  <div style="background: url('img/tokyo.jpg') center center / cover no-repeat fixed;">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-9">

          <div class="card p-4" style="background-color: rgba(255,255,255,0.9)">
            <div class="card-body text-center">
              <div>
                <h1 class="mb-4">プロジェクトベースで働きませんか？</h1>
                私達「firms（ファームス）」は、経験と知見を持つプロフェッショナルが、<br />
                自由に働き、かつ高い収入を確保できる体制づくりを行っています。<br />
                <br />
                プロジェクトを柔軟に設計（週3日～、1ヶ月～ 等）することで、<br />
                皆さんのライフ・スタイルやキャリア・ビジョンに合わせた参画が可能です。<br />
                <br />
                「firms（ファームス）」に実際に参画して下さっているメンバーの中には、

                <div class="row">
                  <div class="col-xl-2 col-lg-1">
                  </div>
                  <div class="col-xl-10 col-lg-11 mt-3 text-left">
                    <ul>
                      <li>ＮＰＯ活動とコンサルティングを両立させている方</li>
                      <li>大学院で研究をしながら、クライアントの問題解決に携わっている方</li>
                      <li>家庭と仕事とのバランスを重視した週３日シフトでコミットしている方</li>
                      <li>海外と日本との２拠点生活を行っている方</li>
                      <li>起業しながら、その資金を得るためにプロジェクトを回している方</li>
                      <li>引き継いだ家業を経営しながらコンサルタントとしての能力を磨いている方</li>
                    </ul>
                  </div>
                </div>

                などがいらっしゃいます。<br />
                <br />
                クライアントは、ベンチャー企業のオーナーや中堅企業の幹部や社長、<br />
                大企業の事業部や子会社、外資系企業が中心で、ダイナミズムを味わえます。<br />
                <br />
                私達が提供しているフィー水準のシミュレーションをご用意しました。<br />
                どうぞお試しください。<br />
                <br />
                「firms（ファームス）」にご賛同を頂けましたら、エントリーして下さい。<br />
                <br />
              </div>
            </div>
          </div>

          <div class="card p-4" style="background-color: rgba(255,255,255,0.9)">
            <form method="post" action="{{route('registration')}}">
            @csrf
            <div class="card-body text-center">
              <div>
                <h2>今までのファーム歴をご記入ください</h2>
                <table class="table table-sm table-responsive">
                  <tr id="tr_firm1">
                    <th><label for="member_ファーム名">ファーム名</label></th>
                    <th><label for="member_在籍期間">在籍期間</label></th>
                  </tr>
                  <tr>
                    <td><select class="form-control" name="member_firm1" id="member_firm1" required>
                      @foreach ($firms as $row)
                        <option value="{{$row->id}}" name="member_firm1">{{$row->firm}}</option>
                      @endforeach
                      </select></td>
                    <td><select class="form-control" name="member_term1" id="member_term1">
                      <option value="">-</option>  
                      @foreach ($terms as $rows)
                           <option value="{{$rows->id}}" name="member_term1">{{$rows->term}}</option> 
                        @endforeach
                      </select>
                      @error('member_term1')
                      <span class="text-danger"><strong>{{ $message }}</strong></span>
                    @enderror
                    </td>
                    
                  </tr>
                  <tr id="tr_firm2" style="display: none;">
                    <td><select class="form-control" name="member_firm2" id="member_firm2">
                      <option value="">-</option>
                      @foreach ($firms as $row)
                        <option value="{{$row->id}}" name="member_firm2">{{$row->firm}}</option>
                      @endforeach
                      </select></td>
                    <td><select class="form-control" name="member_term2" id="member_term2">
                      <option value="">-</option>
                      @foreach ($terms as $rows)
                        <option value="{{$rows->id}}" name="member_term2">{{$rows->term}}</option> 
                      @endforeach
                      </select></td>
                  </tr>
                  <tr id="tr_firm3" style="display: none;">
                    <td><select class="form-control" name="member_firm3" id="member_firm3">
                      <option value="">-</option>
                      @foreach ($firms as $row)
                        <option value="{{$row->id}}" name="member_firm3">{{$row->firm}}</option>
                      @endforeach
                      </select></td>
                    <td><select class="form-control" name="member_term3" id="member_term3">
                        <option value="">-</option>
                        @foreach ($terms as $rows)
                          <option value="{{$rows->id}}" name="member_term3">{{$rows->term}}</option> 
                        @endforeach
                      </select></td>
                  </tr>
                  <tr id="tr_firm4" style="display: none;">
                    <td><select class="form-control" name="member_firm4" id="member_firm4">
                      <option value="">-</option>
                      @foreach ($firms as $row)
                        <option value="{{$row->id}}" name="member_firm4">{{$row->firm}}</option>
                      @endforeach
                      </select></td>
                    <td><select class="form-control" name="member_term4" id="member_term4">
                        <option value="">-</option>
                        @foreach ($terms as $rows)
                          <option value="{{$rows->id}}" name="member_term4">{{$rows->term}}</option> 
                        @endforeach
                      </select></td>
                  </tr>
                  <tr id="tr_firm5" style="display: none;">
                    <td><select class="form-control" name="member_firm5" id="member_firm5">
                      <option value="">-</option>
                      @foreach ($firms as $row)
                      <option value="{{$row->id}}" name="member_firm5">{{$row->firm}}</option>
                    @endforeach
                      </select></td>
                    <td><select class="form-control" name="member_term5" id="member_term5">
                        <option value="">-</option>
                        @foreach ($terms as $rows)
                          <option value="{{$rows->id}}" name="member_term5">{{$rows->term}}</option> 
                        @endforeach
                      </select></td>
                  </tr>
                </table>
                <div class="ri mb-4">
                  <input type="button" class="btn btn-primary" value="ファーム歴追加(5つまで)" id="firm_add">
                </div>
                <div class="ri">
                  <span id="lbl_base_pji" class="my-4"></span><br />
                  <input type="button" class="btn btn-primary" value="あなたの実績から想定される年収を見る" id="base_pji_update">
                </div>

              </div>
              <!--
            </div>
          </div>


          <div class="card p-4">
            <div class="card-body text-center">
-->
              <div class="mt-5" id="reg" style="display: none;">
                <h2>ご登録</h2>
                <p class="text-muted">まずはお気軽にご登録ください。</p>
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                <div class="input-group">
                  <span class="input-group-addon"><i class="icon-user"></i></span>
                  <input type="text" name="name" class="form-control" placeholder="Name">
                </div>
                  @if ($errors->has('name'))
                  <span class="float-left help-block text-danger err"><strong>{{ $errors->first('name') }}</strong></span><br>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="input-group mt-3">
                  <span class="input-group-addon"><i class="icon-envelope"></i></span>
                  <input type="email" class="form-control" name="email" placeholder="E-Mail">
                </div>
                @if ($errors->has('email'))
                <span class="float-left help-block text-danger err"><strong>{{ $errors->first('email') }}</strong></span><br>
                @endif
              </div>
              <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="input-group mt-3">
                  <span class="input-group-addon"><i class="icon-lock"></i></span>
                  <input type="password" name="password" class="form-control" placeholder="Password">
                </div>
                @if ($errors->has('password'))
                <span class="float-left help-block text-danger err"><strong>{{ $errors->first('password') }}</strong></span><br>
                @endif
              </div>
                <div class="row">
                  <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary text-white active mt-3">登録</button>
                  </div>
                  <div class="col-sm-6">
                    <a href="{{route('google_login')}}" type="submit" class="btn btn-primary active mt-3">
                      Googleアカウントで登録
                    </a>
                    {{-- <button type="submit" class="btn btn-primary active mt-3" value="{{route('google_login')}}">Googleアカウントで登録</button> --}}
                  </div>
                </div>
                </form>
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </div>
  <div class="bg-firms">
    <div class="container">
      <div class="row">
        <div class="col-md-6 text-center">
          <a class="btn btn-firms" href="{{ route('login') }}">LogIn</a> <a class="btn btn-firms" href="#">運営会社</a>
        </div>
        <div class="col-md-6 my-2 text-right">
          Copyright © Shares, Inc. 2020
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap and necessary plugins -->
  <script src="{{ asset('vendors/js/jquery.min.js') }}"></script>
  <script src="{{ asset('vendors/js/popper.min.js') }}"></script>
  <script src="{{ asset('vendors/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/option.js') }}"></script>
  <script>
    $(document).ready(function(){
      var err = $('.err').text();
      if(err){
        $('#reg').show();
        $('#reg').focus();
      }
    });
  </script>
</body>

</html>