@extends('layouts/app')
@section('content')
    
    <!-- Main content -->
    <main class="main">

      <!-- Breadcrumb -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">ダッシュボード</li>
      </ol>

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card p-5">
              <h4>こんにちは、ファームス太郎様</h4>
                <table class="table table-responsive table-bordered">
                  <tr class="bg-firms">
                    <th>最新の募集中案件</th><th>現在応募中の案件</th><th>受注した案件</th>
                  </tr>
                  <tr>
                    <td><a href="#">7/21更新：不動産業界におけるアポスティーユ導入について</a></td>
                    <td><a href="#">7/18：アジア圏の販路拡大を踏まえたミャンマーにおける物流拠点構築について</a></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td><a href="#">7/20更新：欧州のデジタルマーケティング戦略立案について</a></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td><a href="#">7/19更新：関西圏某自治体に対する伝染病対策における接触管理アプリ導入について</a></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td><a class="btn btn-primary" href="{{route('projects')}}">現在募集中の案件を見る</a></td>
                    <td><a class="btn btn-primary" href="#">現在応募中の案件を見る</a></td>
                    <td><a class="btn btn-primary" href="#">受注した案件を見る</a></td>
                  </tr>                  
                </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.conainer-fluid -->
    </main>

  </div>
@endsection
