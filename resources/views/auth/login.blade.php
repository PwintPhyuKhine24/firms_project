<!DOCTYPE html>
<html lang="ja">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="プロジェクトベースで働こう。様々なバックグラウンドをもつプロフェッショナルが少しずつ時間を割き、様々な視点を持ち寄ることで、新しい問題解決のプラットフォームを提供します。firms">
  <link rel="shortcut icon" href="img/favicon.png">

  <title>firm（ファームス）|プロジェクトベースで働く新しいコンサルタントのライフスタイル提案</title>

  <!-- Icons -->
  <link href="{{ asset('vendors/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/css/simple-line-icons.min.css') }}" rel="stylesheet">

  <!-- Main styles for this application -->
  <link href="{{ asset('css/style.css')}}" rel="stylesheet">

</head>

<body class="app flex-row align-items-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card-group mb-0">
          <div class="card m-4">
            <div class="card-header bg-firms">
              <img src="img/logo.png" alt="firms"/>
            </div>
            <div class="card-body">
              <h1>Login</h1>
              <p class="text-muted">ログインはこちらから</p>
              <form action="{{ route('memberLogin') }}" method="POST">
                @csrf
                <div class="input-group">
                  <span class="input-group-addon"><i class="icon-envelope"></i></span>
                  <input type="text" name="email" class="form-control" placeholder="E-Mail">
                </div>
                <div class="error">
                  @error('email')
                    <span class="text-danger"><strong>{{ $message }}</strong></span>
                  @enderror
                </div>
                <div class="input-group mt-3">
                  <span class="input-group-addon"><i class="icon-lock"></i></span>
                  <input type="password" name="password" class="form-control" placeholder="Password">
                </div>
                <div class="error">
                  @error('password')
                    <span class="text-danger"><strong>{{ $message }}</strong></span>
                  @enderror
                  @if(Session::has('msg'))
                      <span class="text-danger"><strong>{{ Session::get('msg') }}</strong></span>
                  @endif
                </div>
                <div class="row  mt-3">
                  <div class="col-6">
                    <button type="submit" class="btn btn-firms px-4 text-white">ログイン</button>
                  </div>
                  <div class="col-6 text-right">
                    <button type="button" class="btn btn-link px-0">パスワードを忘れた方はこちらから</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- <a href="client_main.html">デモ用：クライアントメニュー確認</a> | <a href="admin_main.html">デモ用：Adminメニュー確認</a> -->

      </div>
    </div>
  </div>

  <!-- Bootstrap and necessary plugins -->
  <script src="{{ asset('vendors/js/jquery.min.js') }}"></script>
  <script src="{{ asset('vendors/js/popper.min.js') }}"></script>
  <script src="{{ asset('vendors/js/bootstrap.min.js') }}"></script>

</body>
</html>