@extends('layouts/app')
@section('content')

    <!-- Main content -->
    <main class="main">

        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item"><a href="projects.html">案件一覧</a></li>
            <li class="breadcrumb-item active">プロダクト事業における顧客満足度向上推進</li>
        </ol>

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card p-5">
                        <h4>プロダクト事業における顧客満足度向上推進 詳細情報</h4>
                        <table class="table table-responsive table-bordered table-hovered">
                            <tr>
                                <th class="bg-firms" width="30%">案件名</th>
                                <td width="70%">プロダクト事業における顧客満足度向上推進</td>
                            </tr>
                            <tr>
                                <th class="bg-firms">クライアント名</th>
                                <td>某大手通信企業</td>
                            </tr>
                            <tr>
                                <th class="bg-firms">案件受注確度</th>
                                <td>80％</td>
                            </tr>
                            <tr>
                                <th class="bg-firms">フィーサイズ</th>
                                <td>700,000円/月</td>
                            </tr>
                            <tr>
                                <th class="bg-firms">工数</th>
                                <td>5日／週　×　1名</td>
                            </tr>
                            <tr>
                                <th class="bg-firms">期間</th>
                                <td>2013年4月から3ヶ月間程度</td>
                            </tr>
                            <tr>
                                <th class="bg-firms">業務場所</th>
                                <td>東京都港区 / 神奈川県川崎市</td>
                            </tr>
                            <tr>
                                <th class="bg-firms">経験</th>
                                <td>&gt; 事業戦略 − 実行サポート<br>
                                    &gt; マーケティング − 顧客・製品分析<br>
                                    &gt; 収益改善（バリューアップ） − コスト削減機会の分析<br>
                                    &gt; 収益改善（バリューアップ） − 実行サポート<br>
                                    &gt; 改革のファシリテーション<br>
                                </td>
                            </tr>
                            <tr>
                                <th class="bg-firms">条件</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th class="bg-firms">案件概要</th>
                                <td>■ポジション<br />
                                    <br />
                                    ■プロジェクト概要：<br />
                                    顧客の声をもとにアフターサポート（問い合わせ受付・修理）領域の業務改善点の抽出および解決策の検討、推進を行う。<br />
                                    <br />
                                    ■求める人材像：<br />
                                    ・顧客データの分析ができ、課題の抽出および改善策の検討がゼロから行える<br />
                                    ・決定した施策について他部署を巻き込みながら推進できる<br />
                                    <br />
                                    ■フィーサイズ目安：<br />
                                    ・シニアスタッフクラス 70万円/月〜</td>
                            </tr>
                            <tr>
                                <th class="bg-firms">連絡先</th>
                                <td>株式会社@@@@ マネージャー Masanori Katsura / m_kataura@zeus.asia</td>
                            </tr>
                            <tr>
                                <th class="bg-firms">案件ステータス</th>
                                <td>
                                    応募受付中
                                </td>
                            </tr>
                            <tr>
                                <th class="bg-firms">現在の申込み状況</th>
                                <td>
                                    <div class="callout callout-info">
                                        <small class="text-muted">申込者数</small>
                                        <br>
                                        <strong class="h3">2</strong>名
                                    </div>
                                    <table class="table">
                                        <tr class="bg-firms">
                                            <th>ファーム歴</th>
                                            <th>希望時間単価</th>
                                            <th>アベイラビリティ</th>
                                            <th>コメント</th>
                                            <th>連絡</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                ・アクセンチュア/２年以上<br />
                                            </td>
                                            <td>5000円／時間</td>
                                            <td>週3日</td>
                                            <td>週5は難しいですが、3ヶ月でのオペレーション改善であれば、週3で十分まかなえるかと思います。</td>
                                            <td><button class="btn btn-primary"><a href="#"
                                                        class="text-white">連絡を希望する</a></button></td>
                                        </tr>

                                        <tr>
                                            <td>
                                                ・アクセンチュア(戦略グループ)/1年以上<br />
                                                ・ボストン・コンサルティング グループ/3年以上<br />
                                            </td>
                                            <td>10000円／時間</td>
                                            <td>週2日</td>
                                            <td>過去、某携帯大手の事業戦略部と共に、社内のオペレーション改善プロジェクトを指揮したことがあるため、知見が活かせるのでは、と思います。</td>
                                            <td><button class="btn btn-primary"><a href="#"
                                                        class="text-white">連絡を希望する</a></button></td>
                                        </tr>
                                    </table>


                                    【参考】システム管理者の場合
                                    <table class="table">
                                        <tr class="bg-firms">
                                            <th>ファーム歴</th>
                                            <th>希望時間単価</th>
                                            <th>アベイラビリティ</th>
                                            <th>コメント</th>
                                            <th>連絡</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                ・アクセンチュア/２年以上<br />
                                            </td>
                                            <td>5000円／時間</td>
                                            <td>週3日</td>
                                            <td>週5は難しいですが、3ヶ月でのオペレーション改善であれば、週3で十分まかなえるかと思います。</td>
                                            <td><a href="#">Phone Myint Thu</a></td>
                                        </tr>

                                        <tr>
                                            <td>
                                                ・アクセンチュア(戦略グループ)/1年以上<br />
                                                ・ボストン・コンサルティング グループ/3年以上<br />
                                            </td>
                                            <td>10000円／時間</td>
                                            <td>週2日</td>
                                            <td>過去、某携帯大手の事業戦略部と共に、社内のオペレーション改善プロジェクトを指揮したことがあるため、知見が活かせるのでは、と思います。</td>
                                            <td><a href="#">桂 正典</a></td>
                                        </tr>
                                    </table>


                                </td>
                            </tr>

                        </table>


                    </div>
                </div>
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

    </div>
@endsection
