@extends('layouts/app')
@section('content')

    <!-- Main content -->
    <main class="main">

        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item"><a href="projects.html">案件一覧</a></li>
            <li class="breadcrumb-item active">プロダクト事業における顧客満足度向上推進</li>
        </ol>

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card p-5">
                      <form action="{{ route('projectUpdate') }}" method="post">
                        @csrf
                        <h4>プロダクト事業における顧客満足度向上推進 詳細情報</h4>
                        <table class="table table-responsive table-bordered table-hovered">
                            <input type="hidden" name="id" value="{{ $project->id }}">
                            <tr>
                                <th class="bg-firms" width="30%">案件名【必須】</th>
                                <td>
                                    <input class="form-control" type="text" value="{{ $project->term ? $project->term : '' }}"
                                        name="project_name" id="project_pj_name" />
                                    ※案件イメージがつかみやすい名前を記載。
                                </td>
                            </tr>
                            <tr>
                                <th class="bg-firms">クライアント名</th>
                                <td>
                                    <input class="form-control" type="text" value="{{ $project->client_name ? $project->client_name : ''}}" name="project_client"
                                        id="project_pj_client" />
                                    ※企業名が公開出来ない場合、「某大手インフラ企業」という形で記載。
                                </td>
                            </tr>
                            <tr>
                                <th class="bg-firms">案件受注確度【必須】</th>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control" name="project_accuracy" id="project_pj_accuracy">
                                            <option value="20" {{ $project->accuracy == '20' ? 'selected' : '' }}>20</option>
                                            <option value="50" {{ $project->accuracy == '50' ? 'selected' : '' }}>50</option>
                                            <option value="70" {{ $project->accuracy == '70' ? 'selected' : '' }}>70</option>
                                            <option value="80" {{ $project->accuracy == '80' ? 'selected' : '' }}>80</option>
                                            <option value="90" {{ $project->accuracy == '90' ? 'selected' : '' }}>90</option>
                                            <option value="100" {{ $project->accuracy == '100' ? 'selected' : '' }}>100</option>
                                        </select>
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th class="bg-firms">フィーサイズ【必須】</th>
                                <td>
                                    <div class="input-group">
                                        <input class="form-control" size="8" type="text" value="{{ $project->fee01 }}"
                                            name="project_fee01" id="project_pj_fee01" />
                                        <span class="input-group-addon"> 円 / </span>

                                        <select class="form-control" name="project_fee02" id="project_pj_fee02">
                                            <option value="1" {{ $project->fee02 == '1' ? 'selected' : '' }}>日</option>
                                            <option value="2" {{ $project->fee02 == '2' ? 'selected' : '' }}>月</option>
                                            <option value="3" {{ $project->fee02 == '3' ? 'selected' : '' }}>プロジェクト全体</option>
                                        </select>
                                    </div>
                                    ※未確定の場合、複数条件指定の場合等、案件概要欄に記載。
                                </td>
                            </tr>
                            <tr>
                                <th class="bg-firms">工数</th>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control" name="project_cost01" id="project_pj_cost01">
                                            <option value="1" {{ $project->cost01 == '1' ? 'selected' : '' }}>1</option>
                                            <option value="2" {{ $project->cost01 == '2' ? 'selected' : '' }}>2</option>
                                            <option value="3" {{ $project->cost01 == '3' ? 'selected' : '' }}>3</option>
                                            <option value="4" {{ $project->cost01 == '4' ? 'selected' : '' }}>4</option>
                                            <option value="5" {{ $project->cost01 == '5' ? 'selected' : '' }}>5</option>
                                            <option value="6" {{ $project->cost01 == '6' ? 'selected' : '' }}>6</option>
                                            <option value="7" {{ $project->cost01 == '7' ? 'selected' : '' }}>7</option>
                                        </select>
                                        <span class="input-group-addon"> 日 ／ 週 × </span>
                                        <select class="form-control" name="project_cost02" id="project_pj_cost02">
                                            <option value="1" {{ $project->cost02 == '1' ? 'selected' : '' }}>1</option>
                                            <option value="2" {{ $project->cost02 == '2' ? 'selected' : '' }}>2</option>
                                            <option value="3" {{ $project->cost02 == '3' ? 'selected' : '' }}>3</option>
                                            <option value="4" {{ $project->cost02 == '4' ? 'selected' : '' }}>4</option>
                                            <option value="5" {{ $project->cost02 == '5' ? 'selected' : '' }}>5</option>
                                            <option value="6" {{ $project->cost02 == '6' ? 'selected' : '' }}>6</option>
                                            <option value="7" {{ $project->cost02 == '7' ? 'selected' : '' }}>7</option>
                                        </select>
                                        <span class="input-group-addon">名</span>

                                    </div>

                                    ※未確定の場合等、案件概要欄に記載。
                                </td>
                            </tr>
                            <tr>
                                <th class="bg-firms">期間【必須】</th>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control" name="project_period01" id="project_pj_period01">
                                            <option value="2020" {{ $project->period01 == '2020' ? 'selected' : '' }}>2020</option>
                                            <option value="2021" {{ $project->period01 == '2021' ? 'selected' : '' }}>2021</option>
                                            <option value="2022" {{ $project->period01 == '2022' ? 'selected' : '' }}>2022</option>
                                        </select>
                                        <span class="input-group-addon"> 年 </span>
                                        <select class="form-control" name="project_period02" id="project_pj_period02">
                                            <option value="1" {{ $project->period02 == '1' ? 'selected' : '' }}>1</option>
                                            <option value="2" {{ $project->period02 == '2' ? 'selected' : '' }}>2</option>
                                            <option value="3" {{ $project->period02 == '3' ? 'selected' : '' }}>3</option>
                                            <option value="4" {{ $project->period02 == '4' ? 'selected' : '' }}>4</option>
                                            <option value="5" {{ $project->period02 == '5' ? 'selected' : '' }}>5</option>
                                            <option value="6" {{ $project->period02 == '6' ? 'selected' : '' }}>6</option>
                                            <option value="7" {{ $project->period02 == '7' ? 'selected' : '' }}>7</option>
                                            <option value="8" {{ $project->period02 == '8' ? 'selected' : '' }}>8</option>
                                            <option value="9" {{ $project->period02 == '9' ? 'selected' : '' }}>9</option>
                                            <option value="10" {{ $project->period02 == '10' ? 'selected' : '' }}>10</option>
                                            <option value="11" {{ $project->period02 == '11' ? 'selected' : '' }}>11</option>
                                            <option value="12" {{ $project->period02 == '12' ? 'selected' : '' }}>12</option>
                                        </select>
                                        <span class="input-group-addon"> 月 から </span>
                                        <select class="form-control" name="project_period03" id="project_pj_period03">
                                            <option value="1週間" {{ $project->period03 == '1週間' ? 'selected' : '' }}>1週間</option>
                                            <option value="2週間" {{ $project->period03 == '2週間' ? 'selected' : '' }}>2週間</option>
                                            <option value="3" {{ $project->period03 == '3週間' ? 'selected' : '' }}>3週間</option>
                                            <option value="4週間" {{ $project->period03 == '4週間' ? 'selected' : '' }}>4週間</option>
                                            <option value="1ヶ月間" {{ $project->period03 == '1ヶ月間' ? 'selected' : '' }}>1ヶ月間</option>
                                            <option value="2ヶ月間" {{ $project->period03 == '2ヶ月間' ? 'selected' : '' }}>2ヶ月間</option>
                                            <option value="3ヶ月間" {{ $project->period03 == '3ヶ月間' ? 'selected' : '' }}>3ヶ月間</option>
                                            <option value="4ヶ月間" {{ $project->period03 == '4ヶ月間' ? 'selected' : '' }}>4ヶ月間</option>
                                            <option value="5ヶ月間" {{ $project->period03 == '5ヶ月間' ? 'selected' : '' }}>5ヶ月間</option>
                                            <option value="6ヶ月間" {{ $project->period03 == '6ヶ月間' ? 'selected' : '' }}>6ヶ月間</option>
                                            <option value="7ヶ月間" {{ $project->period03 == '7ヶ月間' ? 'selected' : '' }}>7ヶ月間</option>
                                            <option value="8ヶ月間" {{ $project->period03 == '8ヶ月間' ? 'selected' : '' }}>8ヶ月間</option>
                                            <option value="9ヶ月間" {{ $project->period03 == '9ヶ月間' ? 'selected' : '' }}>9ヶ月間</option>
                                            <!-- <option value="10ヶ月間" {{ $project->period03 == '100' ? 'selected' : '' }}>10ヶ月間</option> -->
                                            <option value="10ヶ月間" {{ $project->period03 == '10ヶ月間' ? 'selected' : '' }}>10ヶ月間</option>
                                            <option value="11ヶ月間" {{ $project->period03 == '11ヶ月間' ? 'selected' : '' }}>11ヶ月間</option>
                                            <option value="12ヶ月間" {{ $project->period03 == '12ヶ月間' ? 'selected' : '' }}>12ヶ月間</option>
                                        </select>
                                        <span class="input-group-addon"> 程度 </span>
                                    </div>
                                    ※顧客都合により継続更新の可能性がある、1年以上の継続案件など諸条件に付いては、案件概要欄に記載。
                                </td>
                            </tr>
                            <tr>
                                <th class="bg-firms">業務場所</th>
                                <td>
                                    <input class="form-control" type="text" value="{{ $project->location }}"
                                        name="project_location" id="project_pj_location" /><br />
                                    ※「都内」、「赤坂見附付近」、「東京都◯◯区◯◯町1-2-3 △△ビル」など、<br />
                                    公開出来る範囲に応じて記載。
                                </td>
                            </tr>

                            <tr>
                                <th class="bg-firms">経験</th>
                                <td>
                                    応募者に求める業務経験を、下記の中から選択。<br />
                                    ※選択肢に存在しない物があれば、案件概要欄に記載。<br />

                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr class="bg-firms">
                                                <th width="25%">事業戦略</th>
                                                <th width="25%">グローバル戦略</th>
                                                <th width="25%">マーケティング</th>
                                                <th width="25%">収益改善<br />（バリューアップ）</th>
                                            </tr>
                                        </thead>
                                        <tr>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="a_01"
                                                    id="a_01" />市場環境分析</td>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="b_01"
                                                    id="b_01" />中国</td>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="c_01"
                                                    id="c_01" />顧客・製品分析</td>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="d_01"
                                                    id="d_01" />売上向上機会の分析</td>
                                        </tr>
                                        <tr>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="a_02"
                                                    id="a_02" />戦略策定</td>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="b_02"
                                                    id="b_02" />アジア</td>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="c_02"
                                                    id="c_02" />ターゲティング<br />・ポジショニング</td>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="d_02"
                                                    id="d_02" />コスト削減機会の分析</td>
                                        </tr>
                                        <tr>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="a_03"
                                                    id="a_03" />実行サポート</td>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="b_03"
                                                    id="b_03" />欧州</td>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="c_03"
                                                    id="c_03" />実行サポート</td>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="d_03"
                                                    id="d_03" />実行サポート</td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr class="bg-firms">
                                                <th width="25%">M&A</th>
                                                <th width="25%">経営管理体制の確立</th>
                                                <th colspan=2 width="50%">その他</th>
                                            </tr>
                                        </thead>
                                        <tr>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="e_01"
                                                    id="e_01" />M&A戦略</td>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="f_01"
                                                    id="f_01" />管理会計・KPI設計</td>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="g_01"
                                                    id="g_01" />システム構築（要件定義、構築実務）</td>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="g_04"
                                                    id="g_04" />トレーニング・研修講師</td>
                                        </tr>
                                        <tr>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="e_02"
                                                    id="e_02" />デューデリジェンス、企業価値評価</td>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="f_02"
                                                    id="f_02" />人事制度構築</td>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="g_02"
                                                    id="g_02" />エグゼクティブコーチング</td>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="g_05"
                                                    id="g_05" />プロジェクトアシスタント<br />（議事録取り、分析作業、連絡、事務局係）</td>
                                        </tr>
                                        <tr>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="e_03"
                                                    id="e_03" />統合プロセス</td>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="f_03"
                                                    id="f_03" />組織設計</td>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="g_03"
                                                    id="g_03" />改革のファシリテーション</td>
                                            <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                    name="project_experience[]" type="checkbox" value="g_06"
                                                    id="g_06" />その他</td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>

                            <tr>
                                <th class="bg-firms">条件</th>
                                <td>
                                    応募者に求める条件を、下記の中から選択。<br />
                                    ※選択肢に存在しない物があれば、案件概要欄に記載。<br />

                                    <table class="table table-bordered table-striped">
                                        <tr class="bg-firms">
                                            <th width="33%">語学</th>
                                            <th width="34%">PCスキル</th>
                                            <th width="33%">マネジメントスキル</th>
                                        </tr>
                                        <tr>
                                            <td><input name="project[pj_condition][]" type="hidden" value="" /><input
                                                    name="project_condition[]" type="checkbox" value="a_01"
                                                    id="a_01_condition" />英語</td>
                                            <td><input name="project[pj_condition][]" type="hidden" value="" /><input
                                                    name="project_condition[]" type="checkbox" value="b_01"
                                                    id="b_01_condition" />パワーポイント</td>
                                            <td><input name="project[pj_condition][]" type="hidden" value="" /><input
                                                    name="project_condition[]" type="checkbox" value="c_01"
                                                    id="c_01_condition" />新人指導能力</td>
                                        </tr>
                                        <tr>
                                            <td><input name="project[pj_condition][]" type="hidden" value="" /><input
                                                    name="project_condition[]" type="checkbox" value="a_02"
                                                    id="a_02_condition" />中国語</td>
                                            <td><input name="project[pj_condition][]" type="hidden" value="" /><input
                                                    name="project_condition[]" type="checkbox" value="b_02"
                                                    id="b_02_condition" />エクセル</td>
                                            <td><input name="project[pj_condition][]" type="hidden" value="" /><input
                                                    name="project_condition[]" type="checkbox" value="c_02"
                                                    id="c_02_condition" />顧客折衝能力</td>
                                        </tr>
                                        <tr>
                                            <td><input name="project[pj_condition][]" type="hidden" value="" /><input
                                                    name="project_condition[]" type="checkbox" value="a_03"
                                                    id="a_03_condition" />その他</td>
                                            <td><input name="project[pj_condition][]" type="hidden" value="" /><input
                                                    name="project_condition[]" type="checkbox" value="b_03"
                                                    id="b_03_condition" />簡単なプログラミング</td>
                                            <td><input name="project[pj_condition][]" type="hidden" value="" /><input
                                                    name="project_condition[]" type="checkbox" value="c_03"
                                                    id="c_03_condition" />その他</td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>

                            <tr>
                                <th class="bg-firms">案件概要</th>
                                <td>
                                    <textarea class="form-control" maxlength="10000" name="project_overview"
                                        id="project_pj_overview">
                                      {{ $project->overview }}
                                    </textarea>
                                    ※10,000文字まで<br />
                                    ※案件概要以外にも、案件意義、バリューポイント（コンサルタントの提供する価値）、<br />
                                    留意事項、論点（イシュー）、仮説、検証方法、プロジェクトステップ等を記載。<br />
                                </td>
                            </tr>
                            <tr>
                                <th class="bg-firms">連絡先</th>
                                <td>
                                    <input class="form-control" type="text"
                                        value="{{ $project->contact }}"
                                        name="project_contact" id="project_pj_contact" /><br />
                                    ※本件に関して、連絡の取れる担当者の電話番号、メールアドレス等を記載。
                                </td>
                            </tr>
                            <tr>
                                <th class="bg-firms">案件ステータス</th>
                                <td>
                                    <select class="form-control" name="project_status" id="project_pj_status">
                                        <option value="">以下よりご選択下さい</option>
                                        <option value="1" {{ $project->status == '1' ? 'selected' : '' }}>下書き（非表示）</option>
                                        <option value="2" {{ $project->status == '2' ? 'selected' : '' }}>応募受付中</option>
                                        <option value="3" {{ $project->status == '3' ? 'selected' : '' }}>締切り／審査中</option>
                                        <option value="4" {{ $project->status == '4' ? 'selected' : '' }}>完了（非表示）</option>
                                    </select>
                                </td>
                            </tr>
                        </table>

                        <button class="btn btn-lg btn-firms text-white w-100" type="submit">案件情報を更新する</button>

                      </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

    </div>
    <script>     
      var projects = {!! json_encode($project->toArray()) !!};
      var exp = projects['experience'];
      var condition = projects['condition'];
      var expData = exp.split(',');
      var conData = condition.split(',');
      for(i = 0; i < expData.length; i++)
      {
        switch(expData[i])
        {
          case 'a_01':
            document.getElementById('a_01').checked = true;
            break;
          case 'b_01':
            document.getElementById('b_01').checked = true;
            break;
          case 'c_01':
            document.getElementById('c_01').checked = true;
            break;
          case 'd_01':
            document.getElementById('d_01').checked = true;
            break;

          case 'a_02':
            document.getElementById('a_02').checked = true;
            break;
          case 'b_02':
            document.getElementById('b_02').checked = true;
            break;
          case 'c_02':
            document.getElementById('c_02').checked = true;
            break;
          case 'd_02':
            document.getElementById('d_02').checked = true;
            break;
          
          case 'a_03':
            document.getElementById('a_03').checked = true;
            break;
          case 'b_03':
            document.getElementById('b_03').checked = true;
            break;
          case 'c_03':
            document.getElementById('c_03').checked = true;
            break;
          case 'd_03':
            document.getElementById('d_03').checked = true;
            break;
          
          case 'e_01':
            document.getElementById('e_01').checked = true;
            break;
          case 'f_01':
            document.getElementById('f_01').checked = true;
            break;
          case 'g_01':
            document.getElementById('g_01').checked = true;
            break;
          case 'g_04':
            document.getElementById('g_04').checked = true;
            break;

          case 'e_02':
            document.getElementById('e_02').checked = true;
            break;
          case 'f_02':
            document.getElementById('f_02').checked = true;
            break;
          case 'g_02':
            document.getElementById('g_02').checked = true;
            break;
          case 'g_05':
            document.getElementById('g_05').checked = true;
            break;
          
          case 'e_03':
            document.getElementById('e_03').checked = true;
            break;
          case 'f_03':
            document.getElementById('f_03').checked = true;
            break;
          case 'g_03':
            document.getElementById('g_03').checked = true;
            break;
          case 'g_06':
            document.getElementById('g_06').checked = true;
            break;
          default:
            break;
        }
        console.log(expData[i])
        for(j = 0; j< conData.length; j++)
        {
          switch (conData[j]) {
            case 'a_01':
            document.getElementById('a_01_condition').checked = true;
            break;
          case 'b_01':
            document.getElementById('b_01_condition').checked = true;
            break;
          case 'c_01':
            document.getElementById('c_01_condition').checked = true;
            break;
          case 'd_01':
            document.getElementById('d_01_condition').checked = true;
            break;

          case 'a_02':
            document.getElementById('a_02_condition').checked = true;
            break;
          case 'b_02':
            document.getElementById('b_02_condition').checked = true;
            break;
          case 'c_02':
            document.getElementById('c_02_condition').checked = true;
            break;
          case 'd_02':
            document.getElementById('d_02_condition').checked = true;
            break;
          
          case 'a_03':
            document.getElementById('a_03_condition').checked = true;
            break;
          case 'b_03':
            document.getElementById('b_03_condition').checked = true;
            break;
          case 'c_03':
            document.getElementById('c_03_condition').checked = true;
            break;
          case 'd_03':
            document.getElementById('d_03_condition').checked = true;
            break;
          default:
              break;
          }
        }
      }    
    </script>

@endsection
