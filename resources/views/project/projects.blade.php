@extends('layouts/app')
@section('content')
    
    <!-- Main content -->
    <main class="main">

      <!-- Breadcrumb -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">案件一覧</li>
      </ol>

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card p-5">
              <h4>案件一覧</h4>
              <table class="table table-responsive table-bordered table-hovered">
                <tr class="bg-firms">
                  <th>案件名</th>
                  <th>クライアント名</th>
                  <th>案件受注確度</th>
                  <th>フィーサイズ</th>
                  <th>工数</th>
                  <th>期間</th>
                  <th>業務場所</th>
                  <th>登録日</th>
                  <th>最終更新日</th>
                  <th>案件ステータス</th>
                </tr>
                @foreach ($projects as $project)
                <tr>
                  <td><a href="{{route('project',['id' => 0])}}">{{$project->term}}</a></td>
                  <td>{{$project->client_name}}</td>
                  <td>{{$project->accuracy}}％</td>
                  <td>{{$project->fee01}}円／日 程度</td>
                  <td>{{$project->cost01}}日／週　×　{{$project->cost02}}名 程度</td>
                  <td>{{$project->period01}}年{{$project->period02}}月から{{$project->period03}}ヶ月間程度</td>
                  <td>{{$project->location}}</td>
                  <td>{{$project->created_at}}</td>
                  <td>{{$project->updated_at}}</td>
                  <td>
                    @if ($project->status == 1)
                        Draft(hidden)
                    @elseif ($project->status == 2)
                        Under application
                    @elseif ($project->status == 3)
                        Deadline/Under examination
                    @else
                      Completed(hidden)
                    @endif
                  </td>
                </tr>
                @endforeach
              </table>
              <div class="row justify-content-center">
                <div class="col-sm-6">
                  <ul class="pagination">
                      <li class="page-item"><a class="page-link" href="#">Prev</a></li>
                      <li class="page-item active">
                        <a class="page-link" href="#">1</a>
                      </li>
                      <li class="page-item"><a class="page-link" href="#">2</a></li>
                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                      <li class="page-item"><a class="page-link" href="#">4</a></li>
                      <li class="page-item"><a class="page-link" href="#">Next</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.conainer-fluid -->
    </main>

  </div>
@endsection