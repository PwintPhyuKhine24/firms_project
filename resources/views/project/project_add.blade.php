@extends('layouts/app')
@section('content')

    <!-- Main content -->
    <main class="main">

        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item active">案件登録</li>
        </ol>

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card p-5">
                      <form action="{{ route('createProject') }}" method="POST">
                        @csrf
                        <h4>案件登録</h4>
                            <table class="table table-responsive table-bordered table-hovered">
                                <tr>
                                    <th class="bg-firms" width="30%">案件名【必須】</th>
                                    <td>
                                        <input class="form-control" type="text" value="" name="project_name"
                                            id="project_pj_name" />
                                        ※案件イメージがつかみやすい名前を記載。<br>
                                        @error('project_name')
                                        <span class="text-danger"><strong>{{ $message }}</strong></span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <th class="bg-firms">クライアント名</th>
                                    <td>
                                        <input class="form-control" type="text" value="" name="project_client"
                                            id="project_pj_client" />
                                        ※企業名が公開出来ない場合、「某大手インフラ企業」という形で記載。
                                    </td>
                                </tr>
                                <tr>
                                    <th class="bg-firms">案件受注確度【必須】</th>
                                    <td>
                                        <div class="input-group">
                                            <select class="form-control" name="project_accuracy"
                                                id="project_pj_accuracy">
                                                <option value="">以下よりご選択下さい</option>
                                                <option value="20">20</option>
                                                <option value="50">50</option>
                                                <option value="70">70</option>
                                                <option value="80">80</option>
                                                <option value="90">90</option>
                                                <option value="100">100</option>
                                            </select>
                                            <span class="input-group-addon">%</span>
                                          </div>
                                          @error('project_accuracy')
                                            <span class="text-danger"><strong>{{ $message }}</strong></span>
                                          @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <th class="bg-firms">フィーサイズ【必須】</th>
                                    <td>
                                        <div class="input-group">
                                            <input class="form-control" size="8" type="text" value=""
                                                name="project_fee01" id="project_pj_fee01" />
                                            <span class="input-group-addon"> 円 / </span>

                                            <select class="form-control" name="project_fee02" id="project_pj_fee02">
                                                <option value="0">日</option>
                                                <option value="1">月</option>
                                                <option value="2">プロジェクト全体</option>
                                            </select>
                                        </div>
                                        ※未確定の場合、複数条件指定の場合等、案件概要欄に記載。<br>
                                        @error('project_fee01')
                                          <span class="text-danger"><strong>{{ $message }}</strong></span>
                                        @enderror
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <th class="bg-firms">工数</th>
                                    <td>
                                        <div class="input-group">
                                            <select class="form-control" name="project_cost01" id="project_pj_cost01">
                                                <option value="">未確定</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                            </select>
                                            <span class="input-group-addon"> 日 ／ 週 × </span>
                                            <select class="form-control" name="project_cost02" id="project_pj_cost02">
                                                <option value="">未確定</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                            </select>
                                            <span class="input-group-addon">名</span>
                                        </div>
                                        ※未確定の場合等、詳細を案件概要欄に記載。
                                    </td>
                                </tr>
                                <tr>
                                    <th class="bg-firms">期間【必須】</th>
                                    <td>
                                        <div class="input-group">
                                            <select class="form-control" name="project_period01"
                                                id="project_pj_period01">
                                                <option value="2020">2020</option>
                                                <option value="2021">2021</option>
                                                <option value="2022">2022</option>
                                            </select>
                                            <span class="input-group-addon"> 年 </span>
                                            <select class="form-control" name="project_period02"
                                                id="project_pj_period02">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                            <span class="input-group-addon"> 月 から </span>
                                            <select class="form-control" name="project_period03"
                                                id="project_pj_period03">
                                                <option value="">以下よりご選択下さい</option>
                                                <option value="1">1週間</option>
                                                <option value="2">2週間</option>
                                                <option value="3">3週間</option>
                                                <option value="4">4週間</option>
                                                <option value="10">1ヶ月間</option>
                                                <option value="20">2ヶ月間</option>
                                                <option value="30">3ヶ月間</option>
                                                <option value="40">4ヶ月間</option>
                                                <option value="50">5ヶ月間</option>
                                                <option value="60">6ヶ月間</option>
                                                <option value="70">7ヶ月間</option>
                                                <option value="80">8ヶ月間</option>
                                                <option value="90">9ヶ月間</option>
                                                <option value="100">10ヶ月間</option>
                                                <option value="110">11ヶ月間</option>
                                                <option value="120">12ヶ月間</option>
                                            </select>
                                            <span class="input-group-addon"> 程度 </span>
                                        </div>
                                        ※顧客都合により継続更新の可能性がある、1年以上の継続案件など諸条件に付いては、案件概要欄に記載。<br>
                                        @error('project_period03')                                        
                                          <span class="text-danger"><strong>{{ $message }}</strong></span>                                    
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <th class="bg-firms">業務場所</th>
                                    <td>
                                        <input class="form-control" type="text" value="" name="project_location"
                                            id="project_pj_location" /><br />
                                        ※「都内」、「赤坂見附付近」、「東京都◯◯区◯◯町1-2-3 △△ビル」など、<br />
                                        　公開出来る範囲に応じて記載。
                                    </td>
                                </tr>

                                <tr>
                                    <th class="bg-firms">経験</th>
                                    <td>
                                        応募者に求める業務経験を、下記の中から選択。<br />
                                        ※選択肢に存在しない物があれば、案件概要欄に記載。<br />

                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr class="bg-firms">
                                                    <th width="25%">事業戦略</th>
                                                    <th width="25%">グローバル戦略</th>
                                                    <th width="25%">マーケティング</th>
                                                    <th width="25%">収益改善<br />（バリューアップ）</th>
                                                </tr>
                                            </thead>
                                            <tr>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="a_01"
                                                        id="project_pj_experience" />市場環境分析</td>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="b_01"
                                                        id="project_pj_experience" />中国</td>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="c_01"
                                                        id="project_pj_experience" />顧客・製品分析</td>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="d_01"
                                                        id="project_pj_experience" />売上向上機会の分析</td>
                                            </tr>
                                            <tr>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="a_02"
                                                        id="project_pj_experience" />戦略策定</td>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="b_02"
                                                        id="project_pj_experience" />アジア</td>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="c_02"
                                                        id="project_pj_experience" />ターゲティング<br />・ポジショニング</td>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="d_02"
                                                        id="project_pj_experience" />コスト削減機会の分析</td>
                                            </tr>
                                            <tr>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="a_03"
                                                        id="project_pj_experience" />実行サポート</td>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="b_03"
                                                        id="project_pj_experience" />欧州</td>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="c_03"
                                                        id="project_pj_experience" />実行サポート</td>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="d_03"
                                                        id="project_pj_experience" />実行サポート</td>
                                            </tr>
                                        </table>
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr class="bg-firms">
                                                    <th width="25%">M&A</th>
                                                    <th width="25%">経営管理体制の確立</th>
                                                    <th colspan=2 width="50%">その他</th>
                                                </tr>
                                            </thead>
                                            <tr>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="e_01"
                                                        id="project_pj_experience" />M&A戦略</td>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="f_01"
                                                        id="project_pj_experience" />管理会計・KPI設計</td>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="g_01"
                                                        id="project_pj_experience" />システム構築（要件定義、構築実務）</td>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="g_04"
                                                        id="project_pj_experience" />トレーニング・研修講師</td>
                                            </tr>
                                            <tr>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="e_02"
                                                        id="project_pj_experience" />デューデリジェンス、企業価値評価</td>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="f_02"
                                                        id="project_pj_experience" />人事制度構築</td>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="g_02"
                                                        id="project_pj_experience" />エグゼクティブコーチング</td>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="g_05"
                                                        id="project_pj_experience" />プロジェクトアシスタント<br />（議事録取り、分析作業、連絡、事務局係）
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="e_03"
                                                        id="project_pj_experience" />統合プロセス</td>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="f_03"
                                                        id="project_pj_experience" />組織設計</td>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="g_03"
                                                        id="project_pj_experience" />改革のファシリテーション</td>
                                                <td><input name="project[pj_experience][]" type="hidden" value="" /><input
                                                        name="experience[]" type="checkbox" value="g_06"
                                                        id="project_pj_experience" />その他</td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>

                                <tr>
                                    <th class="bg-firms">条件</th>
                                    <td>
                                        応募者に求める条件を、下記の中から選択。<br />
                                        ※選択肢に存在しない物があれば、案件概要欄に記載。<br />

                                        <table class="table table-bordered table-striped">
                                            <tr class="bg-firms">
                                                <th width="33%">語学</th>
                                                <th width="34%">PCスキル</th>
                                                <th width="33%">マネジメントスキル</th>
                                            </tr>
                                            <tr>
                                                <td><input name="project[pj_condition][]" type="hidden" value="" /><input
                                                        name="project_condition[]" type="checkbox" value="a_01"
                                                        id="project_pj_condition" />英語</td>
                                                <td><input name="project[pj_condition][]" type="hidden" value="" /><input
                                                        name="project_condition[]" type="checkbox" value="b_01"
                                                        id="project_pj_condition" />パワーポイント</td>
                                                <td><input name="project[pj_condition][]" type="hidden" value="" /><input
                                                        name="project_condition[]" type="checkbox" value="c_01"
                                                        id="project_pj_condition" />新人指導能力</td>
                                            </tr>
                                            <tr>
                                                <td><input name="project[pj_condition][]" type="hidden" value="" /><input
                                                        name="project_condition[]" type="checkbox" value="a_02"
                                                        id="project_pj_condition" />中国語</td>
                                                <td><input name="project[pj_condition][]" type="hidden" value="" /><input
                                                        name="project_condition[]" type="checkbox" value="b_02"
                                                        id="project_pj_condition" />エクセル</td>
                                                <td><input name="project[pj_condition][]" type="hidden" value="" /><input
                                                        name="project_condition[]" type="checkbox" value="c_02"
                                                        id="project_pj_condition" />顧客折衝能力</td>
                                            </tr>
                                            <tr>
                                                <td><input name="project[pj_condition][]" type="hidden" value="" /><input
                                                        name="project_condition[]" type="checkbox" value="a_03"
                                                        id="project_pj_condition" />その他</td>
                                                <td><input name="project[pj_condition][]" type="hidden" value="" /><input
                                                        name="project_condition[]" type="checkbox" value="b_03"
                                                        id="project_pj_condition" />簡単なプログラミング</td>
                                                <td><input name="project[pj_condition][]" type="hidden" value="" /><input
                                                        name="project_condition[]" type="checkbox" value="c_03"
                                                        id="project_pj_condition" />その他</td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>

                                <tr>
                                    <th class="bg-firms">案件概要</th>
                                    <td>
                                        <textarea class="form-control" maxlength="10000" name="project_overview"
                                            id="project_pj_overview"></textarea>
                                        ※10,000文字まで<br />
                                        ※案件概要以外にも、案件意義、バリューポイント（コンサルタントの提供する価値）、<br />
                                        　留意事項、論点（イシュー）、仮説、検証方法、プロジェクトステップ等を記載。<br />
                                    </td>
                                </tr>
                                <tr>
                                    <th class="bg-firms">連絡先</th>
                                    <td>
                                        <input class="form-control" type="text" value="" name="project_contact"
                                            id="project_pj_contact" /><br />
                                        ※本件に関して、連絡の取れる担当者の電話番号、メールアドレス等を記載。
                                    </td>
                                </tr>
                                <tr>
                                    <th class="bg-firms">案件ステータス</th>
                                    <td>
                                        <select class="form-control" name="project_status" id="project_pj_status">
                                            <option value="">以下よりご選択下さい</option>
                                            <option value="1">下書き（非表示）</option>
                                            <option value="2">応募受付中</option>
                                            <option value="3">締切り／審査中</option>
                                            <option value="4">完了（非表示）</option>
                                        </select>
                                        @error('project_status')
                                        <span class="text-danger"><strong>{{ $message }}</strong></span>
                                        @enderror
                                    </td>
                                </tr>
                            </table>

                            <button type="submit" class="btn btn-lg btn-firms text-white w-100">案件情報を登録する</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

    </div>
@endsection
