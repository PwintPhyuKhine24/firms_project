@extends('layouts/app')
@section('content')
    
    <!-- Main content -->
    <main class="main">

      <!-- Breadcrumb -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">Dashboard</li>
      </ol>

      <div class="container-fluid">
        <div id="ui-view">
          
        </div>
      </div>
      <!-- /.conainer-fluid -->
    </main>

  </div>
@endsection