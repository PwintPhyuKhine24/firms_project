<?php

use Illuminate\Database\Seeder;

class TermsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('terms')->insert([
            [
                'id' => 1,
                'term' => '1年未満',
                'remark' => '',
                'created_at' => now(),
            ],
            [
                'id' => 2,
                'term' => '1年〜2年未満',
                'remark' => '',
                'created_at' => now(),
            ],
            [
                'id' => 3,
                'term' => '2年〜3年未満',
                'remark' => '',
                'created_at' => now(),
            ],
            [
                'id' => 4,
                'term' => '3年〜4年未満',
                'remark' => '',
                'created_at' => now(),
            ],
            [
                'id' => 5,
                'term' => '4年〜5年未満',
                'remark' => '',
                'created_at' => now(),
            ],
            [
                'id' => 6,
                'term' => '5年〜6年未満',
                'remark' => '',
                'created_at' => now(),
            ],
            [
                'id' => 7,
                'term' => '6年〜7年未満',
                'remark' => '',
                'created_at' => now(),
            ],
            [
                'id' => 8,
                'term' => '7年〜8年未満',
                'remark' => '',
                'created_at' => now(),
            ],
            [
                'id' => 9,
                'term' => '8年〜9年未満',
                'remark' => '',
                'created_at' => now(),
            ],
            [
                'id' => 10,
                'term' => '9年〜10年未満',
                'remark' => '',
                'created_at' => now(),
            ],
            [
                'id' => 11,
                'term' => '10年以上',
                'remark' => '',
                'created_at' => now(),
            ],
        ]);
    }
}
