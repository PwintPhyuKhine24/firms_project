<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects__members', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ID');
            $table->unsignedBigInteger('project_id')->comment('プロジェクトID');
            $table->unsignedBigInteger('member_id')->comment('メンバーID');
            $table->longText('comment')->comment('コメント・質問')->nullable();
            $table->longText('remark')->comment('備考')->nullable();
            $table->timestamp('created_at')->comment('登録日時');
            $table->timestamp('updated_at')->comment('最終更新日時')->nullable();
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects__members');
    }
}
