<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terms', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ID');
            $table->string('term', 255)->comment('期間');
            $table->longText('remark')->comment('備考')->nullable();
            $table->timestamp('created_at')->comment('登録日時');
            $table->timestamp('updated_at')->comment('最終更新日時')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terms');
    }
}
