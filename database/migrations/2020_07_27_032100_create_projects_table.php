<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ID');
            $table->integer('member_id')->comment('ID');
            $table->string('term', 255)->comment('案件名');
            $table->string('client_name', 255)->comment('クライアント名')->nullable();
            $table->integer('accuracy')->comment('案件受注確度');
            $table->integer('fee01')->comment('フィーサイズ：単価');
            $table->integer('fee02')->comment('フィーサイズ：単価単位');
            $table->integer('cost01')->comment('工数：アベイラビリティ')->nullable();
            $table->integer('cost02')->comment('工数：人数')->nullable();
            $table->integer('period01')->comment('期間：開始年');
            $table->integer('period02')->comment('期間：開始月');
            $table->integer('period03')->comment('期間：期間');
            $table->string('location', 255)->comment('業務場所')->nullable();
            $table->string('experience', 255)->comment('経験')->nullable();
            $table->string('condition', 255)->comment('条件')->nullable();
            $table->longText('overview')->comment('案件概要')->nullable();
            $table->string('contact', 255)->comment('連絡先')->nullable();
            $table->integer('status')->comment('案件ステータス');
            $table->longText('remark')->comment('【非公開】案件メモ')->nullable();
            $table->timestamp('created_at')->comment('登録日時');
            $table->timestamp('updated_at')->comment('最終更新日時')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
