<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members__options', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ID');
            $table->unsignedBigInteger('member_id')->comment('member_id');
            $table->integer('firm_id01')->comment('ファーム名_01');
            $table->integer('term_id01')->comment('在籍期間_01');
            $table->integer('firm_id02')->comment('ファーム名_02')->nullable();
            $table->integer('term_id02')->comment('在籍期間_02')->nullable();
            $table->integer('firm_id03')->comment('ファーム名_03')->nullable();
            $table->integer('term_id03')->comment('在籍期間_03')->nullable();
            $table->integer('firm_id04')->comment('ファーム名_04')->nullable();
            $table->integer('term_id04')->comment('在籍期間_04')->nullable();
            $table->integer('firm_id05')->comment('ファーム名_05')->nullable();
            $table->integer('term_id05')->comment('在籍期間_05')->nullable();
            $table->string('experience', 255)->comment('今まで経験した業務内容')->nullable();
            $table->string('university', 255)->comment('学歴:大学')->nullable();
            $table->string('faculty', 255)->comment('学歴:大学学部')->nullable();
            $table->string('graduate', 255)->comment('学歴:大学院')->nullable();
            $table->string('speciality', 255)->comment('学歴:大学院専攻')->nullable();
            $table->string('telephone', 255)->comment('電話番号')->nullable();
            $table->string('facebook_id', 255)->comment('Facebook ID')->nullable();
            $table->string('linkedin_id', 255)->comment('LinkedIn ID')->nullable();
            $table->string('wish', 255)->comment('案件希望条件')->nullable();
            $table->integer('unitprice')->comment('希望時間単価')->nullable();
            $table->integer('availability')->comment('アベイラビリティ（稼働可能時間）')->nullable();
            $table->longText('remark')->comment('その他自由記載欄')->nullable();
            $table->timestamp('created_at')->comment('登録日時');
            $table->timestamp('updated_at')->comment('最終更新日時')->nullable();
            $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members__options');
    }
}
