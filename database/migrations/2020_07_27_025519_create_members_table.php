<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ID');
            $table->integer('role')->comment('ロール');
            $table->string('email', 255)->comment('email')->unique();
            $table->string('password', 255)->comment('Password');
            $table->string('name', 255)->comment('氏名');
            $table->unsignedBigInteger('client_id')->comment('企業')->nullable();
            $table->timestamp('created_at')->comment('登録日時');
            $table->timestamp('updated_at')->comment('最終更新日時')->nullable();
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
