<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Members_Option extends Model
{
    protected $fillable = [
    	'member_id',
    	'firm_id01',
    	'term_id01',
    	'firm_id02',
    	'term_id02',
    	'firm_id03',
    	'term_id03',
    	'firm_id04',
    	'term_id04',
    	'firm_id05',
    	'term_id05',
    	'experience',
    	'university',
    	'faculty',
    	'graduate',
    	'speciality',
    	'telephone',
    	'facebook_id',
    	'linkedin_id',
    	'wish',
    	'unitprice',
    	'availability',
    	'remark',
    ];
    // public function setExperienceAttribute($value)
    // {
    //     $this->attributes['experience'] = json_encode($value);
    // }

    // public function getExperienceAttribute($value)
    // {
    //     return $this->attributes['experience'] = json_decode($value);
    // }

    // public function setWishAttribute($value)
    // {
    //     $this->attributes['wish'] = json_encode($value);
    // }

    // public function getWishAttribute($value)
    // {
    //     return $this->attributes['wish'] = json_decode($value);
    // }
}
