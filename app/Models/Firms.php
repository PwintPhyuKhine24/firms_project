<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Firms extends Model
{
    protected $fillable = [
    	'firm',
    	'firm_point',
    	'remark',
    ];
}
