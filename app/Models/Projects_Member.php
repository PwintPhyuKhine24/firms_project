<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Projects_Member extends Model
{
    protected $fillable = [
    	'project_id',
    	'member_id',
    	'comment',
    	'remark',
    ];
}
