<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Project;

class Member extends Model
{
    protected $fillable = [
    	'role',
    	'email',
    	'password',
    	'name',
    	'client_id',
    ];
    public function member()
	{
		return $this->hasOne('App\Models\Members_Option','member_id','id');
	}
    public function client($value='')
	{
		return $this->belongsTo('App\Models\Client');
	}
	public function projects(){
		return $this->belongsToMany(Project::class,'member_project');
	}
}
