<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Member;
class Project extends Model
{
    protected $table = 'projects';
    protected $fillable = [
        'term',
        'member_id',
    	'client_name',
    	'accuracy',
    	'fee01',
    	'fee02',
    	'cost01',
    	'cost02',
    	'period01',
    	'period02',
    	'period03',
    	'location',
    	'experience',
    	'condition',
    	'overview',
    	'contact',
    	'status',
    	'remark',
    ];

    // public function setExperienceAttribute($value)
    // {
    //     $this->attribute['experience'] = json_encode($value);
    // }

    // public function getExperienceAttribute($value)
    // {
    //     return $this->attribute['experience'] = json_decode($value);
    // }

    // public function setConditionAttribute($value)
    // {
    //     $this->attribute['condition'] = json_encode($value);
    // }

    // public function getConditionAttribute($value)
    // {
    //     return $this->attribute['condition'] = json_decode($value);
    // }
    public function members(){
        return $this->belongsToMany(Member::class,'member_project');
    }
}
