<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
    	'client',
    	'remark',
    ];
    
    public function member($value='')
    {
        return $this->hasMany('App\Models\Member');
    }
}
