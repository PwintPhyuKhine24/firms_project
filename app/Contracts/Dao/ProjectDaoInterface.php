<?php
namespace App\Contracts\Dao;

interface ProjectDaoInterface{
    public function createProject($request);
    public function getProjects();
    public function projectById($project_id);
    public function updateProject($request);
}