<?php
namespace App\Contracts\Dao;

interface FirmDaoInterface{
    public function getAllFirms();
}