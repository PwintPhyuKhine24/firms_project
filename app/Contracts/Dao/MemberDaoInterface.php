<?php

namespace App\Contracts\Dao;

interface MemberDaoInterface
{
    public function createMember($request);
    public function createMemberbyGoogle($gUser);
    public function createMemberOption($request,$id);
    public function loginCheck($request);
    public function updateMember($request,$id);
    public function memberById($id);
}