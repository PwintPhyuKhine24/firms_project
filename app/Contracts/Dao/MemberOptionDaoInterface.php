<?php
namespace App\Contracts\Dao;

interface MemberOptionDaoInterface{
    public function getFirmTerm01ById($id);
    public function getFirmTerm02ById($id);
    public function getFirmTerm03ById($id);
    public function getFirmTerm04ById($id);
    public function getFirmTerm05ById($id);
    public function updateOption($request, $id);
    public function getMemberOptionById($id);
}