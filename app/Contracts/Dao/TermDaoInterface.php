<?php
namespace App\Contracts\Dao;

interface TermDaoInterface{
    public function getAllTerms();
}