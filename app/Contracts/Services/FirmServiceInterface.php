<?php
namespace App\Contracts\Services;

interface FirmServiceInterface{
    public function getAllFirms();
}