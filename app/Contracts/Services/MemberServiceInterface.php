<?php

namespace App\Contracts\Services;
interface MemberServiceInterface
{
    public function createMember($request);
    public function createMemberbyGoogle($gUser);
    public function loginCheck($request);
    public function updateMember($request,$id);
    public function memberById($id);
}