<?php
namespace App\Contracts\Services;

interface ProjectServiceInterface{
    public function createProject($request);
    public function getProjects();
    public function projectById($project_id);
    public function updateProject($request);
}