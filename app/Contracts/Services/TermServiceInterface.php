<?php
namespace App\Contracts\Services;

interface TermServiceInterface{
    public function getAllTerms();
}