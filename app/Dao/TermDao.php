<?php

namespace App\Dao;

use App\Contracts\Dao\TermDaoInterface;
use App\Models\Member;
use App\Models\Term;
use App\Models\Members_Option;
use Illuminate\Support\Facades\Hash;

class TermDao implements TermDaoInterface
{
    public function getAllTerms()
    {
        return Term::all();
    }
    public function getAllMo()
    {
        return Members_Option::all();
    }
}