<?php

namespace App\Dao;

use App\Contracts\Dao\FirmDaoInterface;
use App\Models\Member;
use App\Models\Firms;
use Illuminate\Support\Facades\Hash;

class FirmDao implements FirmDaoInterface
{
    public function getAllFirms()
    {
        return Firms::all();
    }
}