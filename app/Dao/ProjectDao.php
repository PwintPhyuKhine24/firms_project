<?php

namespace App\Dao;

use App\Contracts\Dao\ProjectDaoInterface;
use App\Models\Member;
use App\Models\Project;
use Session;
use Illuminate\Support\Facades\Hash;

class ProjectDao implements ProjectDaoInterface
{
    public function createProject($request)
    {
        $loginMember = Session::get('AUTH');
        $data=Project::insert([
            'term' => $request->project_name,
            'member_id' => $loginMember->id,
            'client_name' => $request->project_client,
            'accuracy' => $request->project_accuracy,
            'fee01' => $request->project_fee01,
            'fee02' => $request->project_fee02,
            'cost01' => $request->project_cost01,
            'cost02' => $request->project_cost02,
            'period01' => $request->project_period01,
            'period02' => $request->project_period02,
            'period03' => $request->project_period03,
            'location' => $request->project_location,
            'experience' => implode(",", $request->input('experience')),
            'condition' => implode(",", $request->input('project_condition')),
            'overview' => $request->project_overview,
            'contact' => $request->project_contact,
            'status' => $request->project_status,
            // 'remark' => $input->project_remark,
            
        ]);
        // dd($d);
        // if($data){
        // $pj=new Project;
        // }
        // $pj->members()->attach($loginMember->id);

    }

    public function getProjects()
    {
        return Project::select('*')
                        // ->groupBy('id')
                        ->orderBy('id', 'ASC')
                        ->get();
    }

    public function projectById($project_id)
    {
        return Project::where('id', $project_id)->first();
    }

    public function updateProject($request)
    {   
        $loginMember = Session::get('AUTH');
        Project::where('id', $request->id)->update([
            'term' => $request->project_name,
            'member_id' => $loginMember->id,
            'client_name' => $request->project_client,
            'accuracy' => $request->project_accuracy,
            'fee01' => $request->project_fee01,
            'fee02' => $request->project_fee02,
            'cost01' => $request->project_cost01,
            'cost02' => $request->project_cost02,
            'period01' => $request->project_period01,
            'period02' => $request->project_period02,
            'period03' => $request->project_period03,
            'location' => $request->project_location,
            'experience' => implode(",", $request->input('project_experience')),
            'condition' => implode(",", $request->input('project_condition')),
            'overview' => $request->project_overview,
            'contact' => $request->project_contact,
            'status' => $request->project_status,
            // 'remark' => $input->project_remark,
        ]);
    }
}