<?php

namespace App\Dao;

use App\Contracts\Dao\MemberOptionDaoInterface;
use App\Models\Member;
use App\Models\Client;
use App\Models\Firms;
use App\Models\Term;
use App\Models\Members_Option;
use Illuminate\Support\Facades\Hash;

class MemberOptionDao implements MemberOptionDaoInterface
{
    /**
     * get Firm Name and Term name by MemberId
     *
     * @param [type] $id
     * @return void
     */
    public function getFirmTerm01ById($id)
    {
        return Members_Option::select('firms.firm', 'terms.term')
                            ->where('members__options.member_id', $id)
                            ->join('firms', 'firms.id', 'members__options.firm_id01')
                            ->join('terms', 'terms.id', '=', 'members__options.term_id01')
                            ->first();                            
    }

    public function getFirmTerm02ById($id)
    {
        return Members_Option::select('firms.firm', 'terms.term')
                            ->where('members__options.member_id', $id)
                            ->join('firms', 'firms.id', 'members__options.firm_id02')
                            ->join('terms', 'terms.id', '=', 'members__options.term_id02')
                            ->first();                            
    }

    public function getFirmTerm03ById($id)
    {
        return Members_Option::select('firms.firm', 'terms.term')
                            ->where('members__options.member_id', $id)
                            ->join('firms', 'firms.id', 'members__options.firm_id03')
                            ->join('terms', 'terms.id', '=', 'members__options.term_id03')
                            ->first();                            
    }

    public function getFirmTerm04ById($id)
    {
        return Members_Option::select('firms.firm', 'terms.term')
                            ->where('members__options.member_id', $id)
                            ->join('firms', 'firms.id', 'members__options.firm_id04')
                            ->join('terms', 'terms.id', '=', 'members__options.term_id04')
                            ->first();                            
    }

    public function getFirmTerm05ById($id)
    {
        return Members_Option::select('firms.firm', 'terms.term')
                            ->where('members__options.member_id', $id)
                            ->join('firms', 'firms.id', 'members__options.firm_id05')
                            ->join('terms', 'terms.id', '=', 'members__options.term_id05')
                            ->first();                            
    }


    public function getMemberOptionById($id)
    {
        return Members_Option::where('member_id', $id)->first();
    }


    /**
     * update member option by id
     *
     * @param [type] $request
     * @param [type] $id
     * @return void
     */
    public function updateOption($request, $id)
    {
        Members_Option::where('member_id', $id)->update([
            'experience' => implode(",", $request->input('member_experience')),
            'university'   => $request->member_university,
            'faculty'      => $request->member_faculty,
            'graduate'     => $request->member_graduate,
            'speciality'   => $request->member_speciality,
            'telephone'    => $request->member_telephone,
            'facebook_id'  => $request->member_facebookURL,
            'linkedin_id'  => $request->member_linkedinURL,
            'wish'         => implode(",", $request->input('member_wish')),
            'unitprice'    => $request->member_unitprice,
            'availability' => $request->member_availability,
            'remark'       => $request->member_remark,
        ]);
    }

}