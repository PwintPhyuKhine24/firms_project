<?php

namespace App\Dao;

use App\Contracts\Dao\MemberDaoInterface;
use App\Models\Member;
use App\Models\Members_Option;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class MemberDao implements MemberDaoInterface
{
    /**
     * insert member function
     *
     * @param [type] $request
     * @return void
     */
    public function createMember($request)
    {
        $member = Member::create([
            'role' => 0,
            'email'=> $request->email,
            'password'=>Hash::make($request->password),
            'name' => $request->name,
            'client_id'=> 1,
        ]);
        Members_Option::insert([
            'member_id' => $member->id,
            'firm_id01' => $request->member_firm1,
            'term_id01' => $request->member_term1,
            'firm_id02' => $request->member_firm2,
            'term_id02' => $request->member_term2,
            'firm_id03' => $request->member_firm3,
            'term_id03' => $request->member_term3,
            'firm_id04' => $request->member_firm4,
            'term_id04' => $request->member_term4,
            'firm_id05' => $request->member_firm5,
            'term_id05' => $request->member_term5,
        ]);
 
        return Member::where('id', $member->id)->first();
    }

    /**
     * update member by id
     *
     * @param [type] $request
     * @param [type] $id
     * @return void
     */
    public function updateMember($request,$id)
    {
        if($request->member_pass != null)
        {
            Member::where('id', $id)->update([
                'role' => 0,
                'email' => $request->member_mail,
                'name' => $request->member_name,
                'client_id' => 1,
                'password' => Hash::make($request->member_pass),
            ]);
        }else{
            Member::where('id', $id)->update([
                'role' => 0,
                'email' => $request->member_mail,
                'name' => $request->member_name,
                'client_id' => 1,
            ]);
          
        }
        Members_Option::where('member_id',$id)->update([
            'member_id' => $id,
            'firm_id01' => $request->member_firm1,
            'term_id01' => $request->member_term1,
            'firm_id02' => $request->member_firm2,
            'term_id02' => $request->member_term2,
            'firm_id03' => $request->member_firm3,
            'term_id03' => $request->member_term3,
            'firm_id04' => $request->member_firm4,
            'term_id04' => $request->member_term4,
            'firm_id05' => $request->member_firm5,
            'term_id05' => $request->member_term5,
        ]);
        
    }

    /**
     * google login
     *
     * @param [type] $gUser
     * @return void
     */
    public function createMemberbyGoogle($gUser)
   {
       return $member =Member::create([
           'name' => $gUser->name,
           'email' => $gUser->email,
           'user_id' => $gUser->id, //追加
           'password' => \Hash::make(uniqid()),
           'role' => 0,
           'client_id' => 1
       ]);
   }

   public function createMemberOption($request,$id)
   {
        Members_Option::insert([
         'member_id' => $member->id,
         'firm_id01' => $request->member_firm1,
         'term_id01' => $request->member_term1,
         'firm_id02' => $request->member_firm2,
         'term_id02' => $request->member_term2,
         'firm_id03' => $request->member_firm3,
         'term_id03' => $request->member_term3,
         'firm_id04' => $request->member_firm4,
         'term_id04' => $request->member_term4,
         'firm_id05' => $request->member_firm5,
         'term_id05' => $request->member_term5,
     ]);
 
   }
   /**
    * login check
    *
    * @param [type] $request
    * @return void
    */
    public function loginCheck($request)
    {
        return Member::where('email', $request->email)->first();
    }

    /**
     * get member by Id
     *
     * @param [type] $id
     * @return void
     */
    public function memberById($id)
    {
        return Member::where('id', $id)->first();
    }
}