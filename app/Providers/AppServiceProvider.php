<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //Service
        $this->app->bind('App\Contracts\Services\MemberServiceInterface', 'App\Services\MemberService');
        $this->app->bind('App\Contracts\Services\ClientServiceInterface', 'App\Services\ClientService');
        $this->app->bind('App\Contracts\Services\FirmServiceInterface', 'App\Services\FirmService');
        $this->app->bind('App\Contracts\Services\MemberOptionServiceInterface', 'App\Services\MemberOptionService');
        $this->app->bind('App\Contracts\Services\ProjectMemberServiceInterface', 'App\Services\ProjectMemberService');
        $this->app->bind('App\Contracts\Services\ProjectServiceInterface', 'App\Services\ProjectService');
        $this->app->bind('App\Contracts\Services\TermServiceInterface', 'App\Services\TermService');

        //dao
        $this->app->bind('App\Contracts\Dao\MemberDaoInterface', 'App\Dao\MemberDao');
        $this->app->bind('App\Contracts\Dao\ClientDaoInterface', 'App\Dao\ClientDao');
        $this->app->bind('App\Contracts\Dao\FirmDaoInterface', 'App\Dao\FirmDao');
        $this->app->bind('App\Contracts\Dao\MemberOptionDaoInterface', 'App\Dao\MemberOptionDao');
        $this->app->bind('App\Contracts\Dao\ProjectMemberDaoInterface', 'App\Dao\ProjectMemberDao');
        $this->app->bind('App\Contracts\Dao\ProjectDaoInterface', 'App\Dao\ProjectDao');
        $this->app->bind('App\Contracts\Dao\TermDaoInterface', 'App\Dao\TermDao');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
