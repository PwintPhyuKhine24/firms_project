<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Contracts\Services\MemberServiceInterface;
use App\Contracts\Services\ProjectServiceInterface;
use Illuminate\Support\Facades\Hash;
use Session;
class LoginController extends Controller
{
    private $memberService;
    private $projectService;
    public function __construct(MemberServiceInterface $memberService, ProjectServiceInterface $projectService)
    {
        $this->memberService = $memberService;
        $this->projectService = $projectService;
    }
    
    /**
     * login form 
     *
     * @return void
     */
    public function login()
    {
        return view('auth.login');
    }

    /**
     * login check by input email and password
     *
     * @param Request $request
     * @return void
     */
    public function loginCheck(Request $request){
        $request->validate([
            'email' => 'required|email',
            'password'  => 'required'
        ]);

        $member = $this->memberService->loginCheck($request);
        if($member){
            if(Hash::check($request->password, $member->password))
            {
                Session::put('AUTH', $member);
                return redirect()->route('dashboard');
            }else {
                return redirect()->back()->with(['msg' => 'パスワードが間違っています。!']);
            }
        }else {
            return redirect()->back()->with(['msg' => 'このメールのメンバーは存在しません。!']);
        }
    }

    public function dashboard(){
        if(Session::has('AUTH'))
        {
            $loginUser = Session::get('AUTH');
            if ($loginUser->role == 0) {
                return view('member.member_main');
            }elseif ($loginUser->role == 1) {
                $projects = $this->projectService->getProjects();
                return view('client.client_main', compact('projects'));
            }else {
                return view('admin.admin_main');
            }
        }else {
            return redirect()->route('login');
        }
    }

    public function logout()
    {
        Session::forget('AUTH');
        Session::flush();
        return redirect()->route('login');
    }

}
