<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Contracts\Services\MemberServiceInterface;
use App\Contracts\Services\MemberOptionServiceInterface;
use Socialite;
use App\Models\Member;
use App\Models\Members_Option;
use Session;

class RegisterController extends Controller
{
    private $memberService;

    public function __construct(MemberServiceInterface $memberService,MemberOptionServiceInterface $memberOptionService)
    {
        $this->memberService = $memberService;
        $this->memberOptionService = $memberOptionService;
    }

    public function createMember(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:members',
            'password' => 'required',
            'member_term1' => 'required'
        ]);
        $member = $this->memberService->createMember($request);
        Session::put('AUTH', $member);
        return redirect()->route('profileReg');
    }

    public function createMemberOption(Request $request)
    {
        $request->validate([
            'member_firm1' => 'required|min:3',
            'member_term1' => 'required|min:2',
        ]);
        $memberOption = $this->memberService->createMemberOption($request,$id);
        return redirect()->route('profileReg');
    }

    public  function  redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public  function  handleGoogleCallback(Request $request)
    {
        $gUser  =  Socialite::driver('google')->stateless()->user();
        // Get the user whose email matches 
        $member  =  Member::where('email',  $gUser->email)->first();
        // create new user 
        if ($member  ==  null) {
            $member  =  $this->memberService->createMemberbyGoogle($gUser,$request);
        }
        Session::put('AUTH', $member);
        return redirect()->route('dashboard');
    }
}
