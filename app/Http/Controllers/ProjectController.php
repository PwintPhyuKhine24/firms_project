<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\Services\ProjectServiceInterface;
use App\Contracts\Services\ProjectMemberServiceInterface;
use App\Models\Project;

class ProjectController extends Controller
{
    private $projectService;
    private $projectMemberService;
    public function __construct(ProjectServiceInterface $projectService, ProjectMemberServiceInterface $projectMemberService)
    {
        $this->projectService = $projectService;
        $this->projectMemberService = $projectMemberService;
    }

    /**
     * item information page
     *
     * @return void
     */
    public function itemInfo()
    {
        $projects = $this->projectService->getProjects();
        return view('client.client_main', compact('projects'));
    }

    /**
     * new project creation page
     *
     * @return void
     */
    public function newProject()
    {
        return view('project.project_add');
    }

    /**
     * create project to db
     *
     * @param Request $request
     * @return void
     */
    public function createProject(Request $request)
    {
        $request->validate([
            'project_name' => 'required',
            'project_accuracy'  => 'required',
            'project_fee01'   => 'required',
            'project_fee02'   => 'required',
            'project_period01' => 'required',
            'project_period02' => 'required',
            'project_period03' => 'required',
            'project_status' => 'required',
        ]);
    
        $this->projectService->createProject($request);
        return redirect()->route('itemInfo');
    }

    public function manageProject($project_id)
    {
        $project = $this->projectService->projectById($project_id);
        return view('project.project_manage', compact('project'));
    }

    /**
     * project edit view page by id
     *
     * @param [type] $project_id
     * @return void
     */
    public function editProject($project_id)
    {
        $project = $this->projectService->projectById($project_id);
        return view('project.project_edit', compact('project'));
    }

    /**
     * update project by Id
     *
     * @param Request $request
     * @return void
     */
    public function updateProject(Request $request)
    {
        $request->validate([
            'project_name' => 'required',
            'project_accuracy'  => 'required',
            'project_fee01'   => 'required',
            'project_fee02'   => 'required',
            'project_period01' => 'required',
            'project_period02' => 'required',
            'project_period03' => 'required',
            'project_status' => 'required',
        ]);
        $this->projectService->updateProject($request);
        return redirect()->route('itemInfo');
    }

    public function projects()
    {
        $projects = Project::all();
        return view('project.projects',compact('projects'));
    }


}
