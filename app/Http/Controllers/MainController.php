<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\Services\FirmServiceInterface;
use App\Contracts\Services\TermServiceInterface;

class MainController extends Controller
{
    private $firmService;
    private $termService;
    /**
     * constructor
     *
     * @param FirmServiceInterface $firmService
     * @param TermServiceInterface $termService
     */
    public function __construct(FirmServiceInterface $firmService, TermServiceInterface $termService)
    {
        $this->firmService = $firmService;
        $this->termService = $termService;
    }

    /**
     * home page 
     *
     * @return void
     */
    public function index()
    {
        $terms = $this->termService->getAllTerms();
        $firms = $this->firmService->getAllFirms();
        return view('index',compact('firms','terms'));
    }

}
