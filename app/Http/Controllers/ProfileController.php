<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Contracts\Services\MemberServiceInterface;
use App\Contracts\Services\FirmServiceInterface;
use App\Contracts\Services\TermServiceInterface;
use App\Contracts\Services\MemberOptionServiceInterface;
use DB;
use App\Models\Firms;
use App\Models\Members_Option;
use App\Models\Member;
use App\Models\Term;

class ProfileController extends Controller
{
    private $memberService;
    private $firmService;
    private $termService;
    private $memberoptionService;
    /**
     * constructor
     *
     * @param MemberServiceInterface $memberService
     * @param FirmServiceInterface $firmService
     * @param TermServiceInterface $termService
     */
    public function __construct(MemberServiceInterface $memberService, FirmServiceInterface $firmService, TermServiceInterface $termService, MemberOptionServiceInterface $memberoptionService)
    {
        $this->memberService = $memberService;
        $this->firmService = $firmService;
        $this->termService = $termService;
        $this->memberoptionService = $memberoptionService;
    }
    
    /**
     * profile page
     *
     * @return void
     */
    public function profileEdit()
    {
        $loginMember = Session::get('AUTH');
        $id = $loginMember->id; 
        $firm_term01 = $this->memberoptionService->getFirmTerm01ById($id);  
        $firm_term02 = $this->memberoptionService->getFirmTerm02ById($id);
        $firm_term03 = $this->memberoptionService->getFirmTerm03ById($id);
        $firm_term04 = $this->memberoptionService->getFirmTerm04ById($id);
        $firm_term05 = $this->memberoptionService->getFirmTerm05ById($id);  
        $member_mail = $loginMember->email;
        $member_option = $this->memberoptionService->getMemberOptionById($id);
        return view('profile.profile_edit', compact('member_option', 'firm_term01', 'firm_term02', 'firm_term03', 'firm_term04', 'firm_term05', 'member_mail'));
    }
    
    /**
     * register member option
     *
     * @param Request $request
     * @return void
     */
    public function registerOption(Request $request)
    {
        $loginMember = Session::get('AUTH');
        $id = $loginMember->id;
        $this->memberoptionService->updateOption($request, $id);
        return redirect()->route('dashboard');
    }

    /**
     * update member option
     *
     * @param Request $request
     * @return void
     */
    public function updateOption(Request $request)
    {
        $loginMember = Session::get('AUTH');
        $id = $loginMember->id;
        $this->memberoptionService->updateOption($request, $id);
        return redirect()->route('profileEdit');
    }
    
    /**
     * profile register page 
     *
     * @return void
     */
    public function profileReg()
    {
        $loginMember = Session::get('AUTH');
        $id = $loginMember->id; 
        $firm_term01 = $this->memberoptionService->getFirmTerm01ById($id);  
        $firm_term02 = $this->memberoptionService->getFirmTerm02ById($id);
        $firm_term03 = $this->memberoptionService->getFirmTerm03ById($id);
        $firm_term04 = $this->memberoptionService->getFirmTerm04ById($id);
        $firm_term05 = $this->memberoptionService->getFirmTerm05ById($id);  
        $member_mail = $loginMember->email;
        return view('profile.profile_reg', compact('firm_term01', 'firm_term02', 'firm_term03', 'firm_term04', 'firm_term05', 'member_mail'));
    }

    /**
     * profile edit page
     *
     * @return void
     */
    public function profilebaseEdit()
    {
        $loginMember = Session::get('AUTH');
        $id = $loginMember->id; 
        $members = $this->memberService->memberById($loginMember->id);
        $firm_term01 = $this->memberoptionService->getFirmTerm01ById($id);  
        $firm_term02 = $this->memberoptionService->getFirmTerm02ById($id);
        $firm_term03 = $this->memberoptionService->getFirmTerm03ById($id);
        $firm_term04 = $this->memberoptionService->getFirmTerm04ById($id);
        $firm_term05 = $this->memberoptionService->getFirmTerm05ById($id);  
        $terms = $this->termService->getAllTerms();
        $firms = $this->firmService->getAllFirms();
        $member_options= $this->termService->getAllMo();
        //var_dump($terms);
        //var_dump($firms);
        $member_option = $this->memberoptionService->getMemberOptionById($id);
        //dd($member_option);
    
       // return view('profile.profile_base_edit',compact('firms','terms','members', 'member_option'));
       return view('profile.profile_base_edit', compact('firm_term01', 'firm_term02', 'firm_term03', 'firm_term04', 'firm_term05', 'members','firms','terms','member_option','member_options'));

    }

    /**
     * update member by id
     *
     * @param Request $request
     * @return void
     */
    public function profileBaseUpdate(Request $request)
    {
        $loginMember = Session::get('AUTH');
        $request->validate([
            'member_name' => 'required',
            'member_mail' => 'required|email',
            'member_pass' => '',
            'member_pass_c'=>'same:member_pass',
        ]);
       
        $member = $this->memberService->updateMember($request,$loginMember->id);
        Session::put('AUTH', $member);
        return redirect()->route('profileEdit');
    }

    // public function profileBaseUpdate(Request $request)
    // {
    //     $member = $this->memberoptionService->profileUpdate($request);
    //     return redirect()->route('profileEdit');
    // }

}
