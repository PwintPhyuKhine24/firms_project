<?php 
namespace App\Services;
use App\Contracts\Services\MemberOptionServiceInterface;
use App\Contracts\Dao\MemberOptionDaoInterface;

class MemberOptionService implements MemberOptionServiceInterface
{
    private $memberOptionDao;
    public function __construct(MemberOptionDaoInterface $memberOptionDao)
    {
        $this->memberOptionDao = $memberOptionDao;
    }

    public function getFirmTerm01ById($id)
    {
        return $this->memberOptionDao->getFirmTerm01ById($id);
    }

    public function getFirmTerm02ById($id)
    {
        return $this->memberOptionDao->getFirmTerm02ById($id);
    }

    public function getFirmTerm03ById($id)
    {
        return $this->memberOptionDao->getFirmTerm03ById($id);
    }

    public function getFirmTerm04ById($id)
    {
        return $this->memberOptionDao->getFirmTerm04ById($id);
    }

    public function getFirmTerm05ById($id)
    {
        return $this->memberOptionDao->getFirmTerm05ById($id);
    }

    public function getMemberOptionById($id)
    {
        return $this->memberOptionDao->getMemberOptionById($id);
    }
    public function updateOption($request, $id)
    {
        $this->memberOptionDao->updateOption($request, $id);
    }
}
?>