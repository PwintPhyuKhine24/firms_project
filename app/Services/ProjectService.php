<?php 
namespace App\Services;
use App\Contracts\Services\ProjectServiceInterface;
use App\Contracts\Dao\ProjectDaoInterface;

class ProjectService implements ProjectServiceInterface
{
    private $projectDao;
    public function __construct(ProjectDaoInterface $projectDao)
    {
        $this->projectDao = $projectDao;
    }

    public function createProject($request)
    {
        $this->projectDao->createProject($request);
    }

    public function getProjects()
    {
        return $this->projectDao->getProjects();
    }

    public function projectById($project_id)
    {
        return $this->projectDao->projectById($project_id);
    }

    public function updateProject($request)
    {
        $this->projectDao->updateProject($request);
    }
}
?>