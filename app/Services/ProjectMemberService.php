<?php 
namespace App\Services;
use App\Contracts\Services\ProjectMemberServiceInterface;
use App\Contracts\Dao\ProjectMemberDaoInterface;

class ProjectMemberService implements ProjectMemberServiceInterface
{
    private $projectMemberDao;
    public function __construct(ProjectMemberDaoInterface $projectMemberDao)
    {
        $this->projectMemberDao = $projectMemberDao;
    }
}
?>