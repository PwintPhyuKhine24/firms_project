<?php 
namespace App\Services;
use App\Contracts\Services\FirmServiceInterface;
use App\Contracts\Dao\FirmDaoInterface;

class FirmService implements FirmServiceInterface
{
    private $firmDao;
    public function __construct(FirmDaoInterface $firmDao)
    {
        $this->firmDao = $firmDao;
    }

    public function getAllFirms()
    {
        return $this->firmDao->getAllFirms();
    }
}
?>