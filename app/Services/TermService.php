<?php 
namespace App\Services;
use App\Contracts\Services\TermServiceInterface;
use App\Contracts\Dao\TermDaoInterface;

class TermService implements TermServiceInterface
{
    private $termDao;
    public function __construct(TermDaoInterface $termDao)
    {
        $this->termDao = $termDao;
    }

    public function getAllTerms()
    {
        return $this->termDao->getAllTerms();
    }
    public function getAllMo()
    {
        return $this->termDao->getAllMo();
    }
}
?>