<?php 
namespace App\Services;
use App\Contracts\Services\MemberServiceInterface;
use App\Contracts\Dao\MemberDaoInterface;

class MemberService implements MemberServiceInterface
{
    private $memberDao;
    public function __construct(MemberDaoInterface $memberDao)
    {
        $this->memberDao = $memberDao;
    }

    public function createMember($request)
    {
        return $this->memberDao->createMember($request);
    }

    public function createMemberOption($request,$id)
    {
        $this->memberDao->createMemberbyGoogle($request,$id);
    }
    public function updateMember($request,$id)
    {
        $this->memberDao->updateMember($request,$id);
    }
    
    public function createMemberbyGoogle($gUser)
    {
        return $this->memberDao->createMemberbyGoogle($gUser);
    }

    public function loginCheck($request)
    {
        return $this->memberDao->loginCheck($request);
    }

    public function memberById($id)
    {
        return $this->memberDao->memberById($id);
    }
}