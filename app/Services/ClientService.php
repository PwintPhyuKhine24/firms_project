<?php 
namespace App\Services;
use App\Contracts\Services\ClientServiceInterface;
use App\Contracts\Dao\ClientDaoInterface;

class ClientService implements ClientServiceInterface
{
    private $clientDao;
    public function __construct(ClientDaoInterface $clientDao)
    {
        $this->clientDao = $clientDao;
    }
}
?>